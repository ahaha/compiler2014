package tiger.syntactic;

%%

%unicode
%line
%column
%cup
%public
%implements Symbols

%{
	private int commentCount = 0;

	private StringBuffer string = new StringBuffer();

	private void err(String message) {
		throw new RuntimeException("Scanning error in line " + yyline + ", column " + yycolumn + ": " + message);
	}

	private java_cup.runtime.Symbol tok(int kind) {
		return new java_cup.runtime.Symbol(kind, yyline, yycolumn);
	}

	private java_cup.runtime.Symbol tok(int kind, Object value) {
		return new java_cup.runtime.Symbol(kind, yyline, yycolumn, value);
	}
%}

%eofval{
	{
		switch (yystate()) {
			case MULTI_LINE_COMMENT: {
				err("Comment symbol do not match (EOF)!");
				break;
			}
			case STRING: {
				err("Unexpected end of string");
				break;
			}
			default:
				return tok(EOF, null);
		}
	}
%eofval}

LineTerm = \n|\r|\r\n
Letter = [a-zA-Z_$]
DecDigit = [0-9]
HexDigit = [0-9a-fA-F]
Identifier = {Letter} ({Letter}|{DecDigit})*

OctInteger = "0" [0-7]+
DecInteger = [0-9]+
HexInteger = "0" [xX] {HexDigit}+

OctCharacter = \\ "0" [0-7]{1,3}
HexCharacter = \\ [xX] {HexDigit}{1,2}

Whitespace = {LineTerm}|[ \t\f]
InputCharacter = [^\n\r\\\"\']

%state	MULTI_LINE_COMMENT
%state  SINGLE_LINE_COMMENT
%state  STRING
%state	INCLUDE
%state	DEFINE


%%

<YYINITIAL> {
	"/*" { yybegin(MULTI_LINE_COMMENT); }
	"*/" { err("Comment symbol do not match!"); }

	\" { string.setLength(0); yybegin(STRING); }


	"int"		{ return tok(INT); }
	"if"		{ return tok(IF); }
	"return"	{ return tok(RETURN); }
	"void"		{ return tok(VOID); }
	"char"		{ return tok(CHAR); }
	"struct"	{ return tok(STRUCT); }
	"union"		{ return tok(UNION); }
	"else"		{ return tok(ELSE); }
	"while"		{ return tok(WHILE); }
	"for"		{ return tok(FOR); }
	"continue"	{ return tok(CONTINUE); }
	"break"		{ return tok(BREAK); }
	"sizeof"	{ return tok(SIZEOF); }

	"(" { return tok(LPAREN); }
	")" { return tok(RPAREN); }
	"{" { return tok(LBRACE); }
	"}" { return tok(RBRACE); }
	"[" { return tok(LBRACKET); }
	"]" { return tok(RBRACKET); }

	";" { return tok(SEMICOLON); }
	"," { return tok(COMMA);}
	"." { return tok(DOT); }

	"+" { return tok(PLUS); }
	"-" { return tok(MINUS); }
	"*" { return tok(TIMES); }
	"/" { return tok(DIVIDE); }
	"%" { return tok(MODULO);}

	"=" { return tok(ASSIGN); }

	"|" { return tok(BIT_OR); }
	"^" { return tok(BIT_XOR); }
	"&" { return tok(BIT_AND); }
	"~" { return tok(BIT_NOT); }
	"!"	{ return tok(NOT); }

	"==" { return tok(EQ); }
	"!=" { return tok(NE) ;}
	"<"  { return tok(LT); }
	"<=" { return tok(LE); }
	">"  { return tok(GT); }
	">=" { return tok(GE); }

	"||" { return tok(OR); }
	"&&" { return tok(AND); }
	"<<" { return tok(SHL); }
	">>" { return tok(SHR); }
	"++" { return tok(INC); }
	"--" { return tok(DEC); }
	"->" { return tok(PTR); }

	"*=" { return tok(MUL_ASSIGN); }
	"/=" { return tok(DIV_ASSIGN); }
	"%=" { return tok(MOD_ASSIGN); }
	"+=" { return tok(ADD_ASSIGN); }
	"-=" { return tok(SUB_ASSIGN); }
	"&=" { return tok(AND_ASSIGN);}
	"|=" { return tok(OR_ASSIGN);}
	"^=" { return tok(XOR_ASSIGN); }
	"<<=" { return tok(SHL_ASSIGN); }
	">>=" { return tok(SHR_ASSIGN); }

	"//" { yybegin(SINGLE_LINE_COMMENT); }

	{Identifier} { return tok(ID, yytext()); }
	{HexInteger} { return tok(NUM, Integer.decode(yytext())); }
	{OctInteger} { return tok(NUM, Integer.decode(yytext())); }
	{DecInteger} { return tok(NUM, new Integer(yytext())); }

	\' ({InputCharacter} | \") \' { return tok(CHR, yytext().charAt(1)); }
	"'\\t'" { return tok(CHR, '\t'); }
	"'\\n'" { return tok(CHR, '\n'); }
	"'\\r'" { return tok(CHR, '\r'); }
	"'\\0'" { return tok(CHR, '\0'); }
	\'\\\'\'	{ return tok(CHR, '\''); }
	\'\\\"\'	{ return tok(CHR, '"'); }
	\'\\\\\'	{ return tok(CHR, '\\'); }
	\' {HexCharacter} \' { return tok(CHR, (char)Integer.decode(yytext().replace("'", "").replace('\\', '0')).intValue()); }
	\' {OctCharacter} \' { return tok(CHR, (char)Integer.decode(yytext().replace("'", "").replace('\\', '0')).intValue()); }

	"#"	{ yybegin(INCLUDE); }

	{Whitespace} { /* skip */ }

}

<MULTI_LINE_COMMENT> {
	"*/" { yybegin(YYINITIAL); }
	[^] {}
}

<SINGLE_LINE_COMMENT> {
	{LineTerm} { yybegin(YYINITIAL); }
	[^] {}
}

<STRING> {
	\"	{ yybegin(YYINITIAL); return tok(STR, string.toString()); }
	({InputCharacter} | \')+	{ string.append(yytext()); }
	[\n\r]	{ err("Unexpected end of string " + string); }
	\\t	{ string.append('\t'); }
	\\n	{ string.append('\n'); }
	\\r	{ string.append('\r'); }
	\\\"	{ string.append('\"'); }
	\\\\	{ string.append('\\'); }
}

<DEFINE> {
	{LineTerm}	{ yybegin(YYINITIAL); }
	[^] {}
}

<INCLUDE> {
	{LineTerm}	{ yybegin(YYINITIAL); }
	[^] {}
}

[^] { err("Illegal character :" + yytext());}
