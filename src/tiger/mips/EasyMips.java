package tiger.mips;

import java.util.ArrayList;
import java.util.List;

import tiger.ast.Op.OpType;
import tiger.translate.*;
import tiger.type.*;

public class EasyMips extends BasicMips {


	public EasyMips(ArrayList<ICode> c, List<RecordField> g, int d) throws Exception {
		super(c, g, d);
		gen_data();
		gen_text();
	}
	
	private void gen_data() {
		put(".data");
		put(".newline: .asciiz \"\\n\"");
		put(".align 2");
		for (RecordField r:global_vars) {
			if (r.type instanceof Pointer) {
				Object data = r.val();
				if (data instanceof String) {
					if (!(r.type instanceof Array))
						r.type = new Array(new Char(), data.toString().length() + 1);
					put(".%s: .asciiz \"%s\"", r.symbol, ((String) data).replaceAll("\n", "\\\\" + "n"));
					put(".align 2");
				} else if (data instanceof List){
					put(".%s:", r.symbol);
					for (Object t : (List)data) {
						if (t instanceof Character) {
							put(".byte %d", (int)((Character)t).charValue());
						} else if (t instanceof Integer) {
							put(".word %d", t);
						} else {
							put(".space %d", r.symbol, RecordField.align(((Array)r.type).elementType.size()));
						}
					}
					put(".align 2");
				} else {
					put(".%s: .space %d", r.symbol, RecordField.align(r.type.size()));
				}
			} else if (r.type.size() == 4) {
				put(".%s: .word %d", r.symbol, r.val());
			} else if (r.type instanceof Char) {
				put(".%s: .ascii \"%c\"", r.symbol, r.val());
				put(".align 2");
			} else {
				put(".extern .%s %d", r.symbol, RecordField.align(r.type.size()));
			}
		}
		put("");
	}
	
	private void gen_text() {
		put(".text");
		if (DEBUG >= 2) {
			include("debug.s");
		}
		include("malloc.s");
		include("printf.s");
		for (ICode i: codes) {
			if (i instanceof Label) {
				gen((Label)i);
			} else if (i instanceof Call) {
				gen((Call)i);
			} else if (i instanceof Quad) {
				gen((Quad)i);
			} else if (i instanceof Jump) {
				gen((Jump)i);
			}
		}
		put("");
	}

	private void gen(Label c) {
		if (DEBUG >= 1)
			put("nop #%s", c.name);
		putl(c.name);
		if (c instanceof Proc) {
			if (!c.name.startsWith("_")) {
				puti("sw", fp, sp, -4);
				puti("sw", ra, sp, -8);
				puti("move", fp, sp);
				puti("addiu", sp, sp, -((Proc) c).stack_size);
			} else {
				puti("addiu", sp, sp, ((Proc) c).stack_size);
				puti("lw", fp, sp, -4);
				puti("lw", ra, sp, -8);
				puti("jr", ra);
				put("nop");
				put("nop");
				put("");
				put("");
			}
		}
	}
	
	private void gen(Call c) {
		if (c.args != null) {
			int offset = 0;
			for (Term v : c.args) {
				offset += move(v, sp, offset);
			}
		}
		puti("jal", c.addr.name);
	}
	
	private void gen(Quad c) {
		if (c.op.optype == OpType.MEMOF) return ;
		debug(c.toString());
		load(a0, c.B, c._B);
		if (c.C != null) {
			if (c.op.optype == OpType.COMMA || c.op.optype == OpType.CAST) {
				load(a0, c.C, c._C);
			} else if (c.op.optype == OpType.DIVIDE) {
				load(a1, c.C, c._C);
				puti("div", a0, a1);
				puti("mflo", a0);
			} else if (c.op.optype == OpType.MODULO) {
				load(a1, c.C, c._C);
				puti("div", a0, a1);
				puti("mfhi", a0);
			} else {
				load(a1, c.C, c._C);
				puti(c.op.asmrr(), a0, a0, a1);
			}
		} else {
			if (c.op.optype == OpType.NOT) {
				puti("seq", a0, a0, $0);
			} else if (c.op.optype == OpType.ASSIGN) {
				if (((Variable)c.A).decl.type instanceof Record) {
					load(a1, c.A, false);
					memcpy(a0, a1, ((Variable)c.A).decl.type.size());
					return ;
				}
			} else {
				puti(c.op.asmr(), a0, a0);
			}
		}
		save(a0, (Variable)c.A);
	}
	
	private void gen(Jump c) {
		debug(c.toString());
		if (c.op == null) {
			puti("b", c.t);
		} else if (c.op.optype == OpType.COMMA) {
			load(a0, c.B, true);
			puti("bne", a0, $0, c.t.name);
		} else if (c.op.optype == OpType.CAST) {
			load(a0, c.B, true);
			puti("bne", a0, $0, c.t.name);
		} else if (c.op.optype == OpType.DIVIDE) {
			load(a0, c.A, true);
			load(a1, c.B, true);
			puti("div", a0, a1);
			puti("mflo", a0);
			puti("bne", a0, $0, c.t.name);
		} else if (c.op.optype == OpType.MODULO) {
			load(a0, c.A, true);
			load(a1, c.B, true);
			puti("div", a0, a1);
			puti("mfhi", a0);
			puti("bne", a0, $0, c.t.name);
		}	else {
			load(a0, c.A, true);
			String o = c.op.toString().toLowerCase();
			if (c.op.isCompare()) {
				if (c.B != null) {
					load(a1, c.B, true);
					put("b%s %s %s %s", o, a0, a1, c.t.name);
				} else {
					put("b%s %s %s %s", o, a0, $0, c.t.name);
				}
			} else {
				if (c.B != null) {
					load(a1, c.B, true);
					puti(c.op.asmrr(), a0, a1, a0);
					puti("bne", a0, $0, c.t.name);
				} else if (c.op.optype == OpType.NOT) {
					puti("beq", a0, $0, c.t.name);
				} else throw new Error("TODO JUMP");
			}
		}
	}
	
	private void load(String rd, Term v, boolean is_value) {
		if (v instanceof Scalar)
			load(rd, (Scalar)v);
		else if (v instanceof Variable)
			load(rd, (Variable)v, is_value);
	}
	
	private void load(String rd, Variable v, boolean is_value) { // only load word
		String ins = v.decl.type instanceof Char ? "lb" : "lw";
		if (is_value) {
			if (v.addr instanceof String) {
				if (v.addr.toString().startsWith("$"))
					puti("move", rd, v.addr);
				else
					puti(ins, rd, v.addr);
			} else if (v.addr instanceof Term) {
				load(rd, (Term) v.addr, true);
				puti(ins, rd, rd, 0);
			} else if (v.decl != null) {
				// Local Variable
				puti(ins, rd, v.decl.pos, v.decl.offset);
			}
		} else {
			if (v.addr instanceof String) {
				if (v.addr.toString().startsWith("$"))
					puti("move", rd, v.addr);
				else
					puti("la", rd, v.addr);
			} else if (v.addr instanceof Term) {
				load(rd, (Term) v.addr, true);
			} else if (v.decl != null) {
				puti("la", rd, v.decl.pos, v.decl.offset);
			}
		}
	}
	
	private void load(String rd, Scalar v) {
		if (v.value instanceof Integer) {
			puti("li", rd, (Integer)v.value);
		} else if (v.value instanceof Character) {
			puti("la", rd, "'" + v.value.toString() + "'");
		}
	}
	
	private void save(String rd, Variable v) { // only save word
		String ins = v.decl.type instanceof Char ? "sb" : "sw";
		if (v.addr != null) {
			String reg = use();
			if (v.addr instanceof Variable) {
				load(reg, (Term) v.addr, true);
			} else if (v.addr instanceof String) {
				if (v.addr.toString().startsWith("$")) {
					puti("move", v.addr.toString(), rd);
					free(reg);
					return ;
				} else {
					puti("la", reg, v.addr);
				}
			}
			puti(ins, rd, reg, 0);
			free(reg);
			return ;
		}
		puti(ins, rd, v.decl.pos, v.decl.offset);
	}
	
	private int move(Term v, String rt, Integer offset) {
		if (v instanceof Scalar) {
			load(a0, (Scalar)v);
			puti("sw", a0, rt, offset);
			return 4;
		} else if (v instanceof Variable) {
			if (((Variable) v).decl.type instanceof Array) {
				String reg = use();
				load(reg, v,false);
				puti("sw", reg, rt, offset);
				free(reg);
				return 4;
			}
			if (((Variable) v).decl.type.size() <= 4) {
				String reg = use();
				load(reg, v, true);
				puti("sw", reg, rt, offset);
				free(reg);
				return 4;
			}
			int size = RecordField.align(((Variable)v).decl.type.size());
			for (int now = 0; now < size; now += 4) {
				puti("lw", a0, ((Variable) v).decl.pos, ((Variable) v).decl.offset + now);
				puti("sw", a0, rt, offset + now);
			}
			return size;
		}
		return 0;
	}
	
	private void memcpy(String rs, String rt, Integer size) {
		size = RecordField.align(size);
		String reg = use();
		for (int offset = 0; offset < size; offset += 4) {
			puti("lw", reg, rs, offset);
			puti("sw", reg, rt, offset);
		}
		free(reg);
	}
	
}
