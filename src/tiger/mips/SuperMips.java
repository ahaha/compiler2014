package tiger.mips;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.Stack;

import tiger.ast.Op;
import tiger.ast.Op.OpType;
import tiger.translate.*;
import tiger.type.*;

class Cmp implements Comparator {
	public int compare(Object _a, Object _b) {
		Variable a = (Variable)_a, b = (Variable)_b;
		if (a.begin > b.begin)
			return 1;
		if (a.begin < b.begin)
			return -1;
		if (a.end > b.end)
			return 1;
		if (a.end < b.end)
			return -1;
		return 0;
	}
}

public class SuperMips extends BasicMips {
	
	private ICode cs[];
	private int N;
	private List<Integer> ord;
	private List<Variable> vars;
	private Set<Variable> use[], def[], in[], out[];
	private Set<Integer> pred[], succ[];
	private List<Variable> active;
	private boolean mark[];
	private int queue[];
	
	public SuperMips(ArrayList<ICode> c, List<RecordField> g, int d) throws Exception {
		super(c, g, d);
		gen_data();
		inline();
		scan();
		gen_text();
		spim();
		spim();
	}
	
	private void ID() {
		int cnt = 0;
		for (ICode c : codes)
			c.id = cnt++;
	}
	
	private Object lookup(HashMap<Object, Object> t, Object u) {
		if (u == null)
			return null;
		Object uu = t.get(u);
		if (uu != null)
			return uu;
		if (u instanceof Variable) {
			String pos = ((Variable) u).decl.pos;
			if (pos.equals("$sp") || pos.equals("$fp")) {
				Variable v = new Variable();
				v.addr = lookup(t, ((Variable) u).addr);
				v.decl = new RecordField(((Variable) u).decl.type);
				t.put(u, v);
				return v;
			}
		} else if (u instanceof Label) {
			Label v = new Label();
			t.put(u, v);
			return v;
		}
		return u;
	}
	
	private void inline() {
		List<Proc> fs;
		List<Proc> call[];
		List<Proc> from[];
		fs = new ArrayList<Proc>();
		Proc cur = null;
		int cnt = 0;
		ID();
		for (ICode c: codes)
			if (c instanceof Proc) {
				if (!c.toString().startsWith("_")) {
					fs.add((Proc) c);
					((Proc)c).pid = cnt++;
				}
			}
		N = fs.size();
		call = new ArrayList[N];
		from = new ArrayList[N];
		int[] out = new int[N];
		cnt = 0;
		for (ICode c: codes)
			if (c instanceof Proc) {
				if (!c.toString().startsWith("_")) {
					from[cnt] = new ArrayList<Proc>();
					call[cnt++] = new ArrayList<Proc>();
					cur = (Proc) c;
				} else {
				}
			} else if (c instanceof Call) {
				Proc d = ((Call) c).addr;
				if (d.name.equals("printf") || d.name.equals("malloc"))
					continue;
				call[cur.pid].add(d);
				from[d.pid].add(cur);
				out[cur.pid]++;
			}
		int h = 0, t = 0;
		queue = new int[N];
		for (int i = 0; i < N; i++)
			if (out[i] == 0)
				queue[t++] = i;
		while (h < t) {
			int u = queue[h++];
			for (Proc p : from[u])
				if ((--out[p.pid]) == 0)
					queue[t++] = p.pid;
		}
		if (DEBUG >= 2)
			for (int i = 0; i < t; i++)
				System.err.println(fs.get(queue[i]));
		HashMap<Integer, Object> H = new HashMap<Integer, Object>();
		for (int i = 0; i < t && i < 1; i++) {
			int u = queue[i];
			int begin = fs.get(u).id, end = begin + 1;
			while (!(codes.get(end) instanceof Proc)) end++;
			if (DEBUG >= 1)
				System.err.println("Inlined: " + fs.get(u));
			List<ICode> l = codes.subList(begin, end + 1);
			for (ICode c : codes)
				if (c instanceof Call) {
					if (((Call) c).addr.equals(fs.get(u))) {
						if (DEBUG >= 2)
							System.err.println(c.id + " " + c);
						List<ICode> m = new ArrayList<ICode>();
						HashMap<Object, Object> trans = new HashMap<Object, Object>();
						if (((Call) c).ret != null)
							trans.put(Translate.RetVal, ((Call)c).ret);
						int arg_cnt = 0;
						for (Variable v : fs.get(u).decl) {
							Variable w = (Variable) lookup(trans, v);
							Term a = ((Call) c).args.get(arg_cnt++);
							m.add(new Quad(new Op(OpType.ASSIGN), w, a, Translate.isvalue(a)));
						}
						for (ICode a : l) {
							if (a instanceof Label) {
								m.add((Label)lookup(trans, a));
							} else if (a instanceof Quad) {
								Term A = (Term) lookup(trans, ((Quad) a).A),
									 B = (Term) lookup(trans, ((Quad) a).B),
									 C = (Term) lookup(trans, ((Quad) a).C);
								m.add(new Quad(((Quad) a).op, A, B, ((Quad) a)._B, C, ((Quad) a)._C));
							} else if (a instanceof Jump) {
								Label T = (Label) lookup(trans, ((Jump) a).t);
								Term A = (Term) lookup(trans, ((Jump) a).A),
									 B = (Term) lookup(trans, ((Jump) a).B);
								m.add(new Jump(T, ((Jump) a).op, A, B));
							} else if (a instanceof Call) {
								List<Term> A = new ArrayList<Term>();
								if (((Call) a).args != null)
									for (Term arg : ((Call) a).args)
										A.add((Term) lookup(trans, arg));
								m.add(new Call(((Call) a).addr, A));
							}
						}
						int offset = 0;
						for (Object v : trans.values())
							if (v instanceof Variable) {
								if (((Variable) v).decl.type instanceof Pointer)
									offset += 4;
								else
									offset += RecordField.align(((Variable) v).decl.type.size());
								((Variable) v).decl.offset = cur.temp_size + offset;
							}
						cur.temp_size += offset;
						cur.newarg_size += offset;
						cur.stack_size += offset;
						((Proc)Label.get("_" + cur.name)).stack_size = cur.stack_size;
						H.put(c.id, m);
					}
				} else if (c instanceof Proc) {
					if (!((Proc) c).name.startsWith("_"))
						cur = (Proc) c;
				}
		}
		Object[] inlines = H.keySet().toArray();
		Arrays.sort(inlines, Collections.reverseOrder());
		for (Object k : inlines) {
			int p = ((Integer)k).intValue();
			if (((Call)codes.get(p)).ret != null)
				codes.remove(p + 1);
			codes.remove(p);
			codes.addAll(p, (Collection<? extends ICode>) H.get(k));
		}
	}

	private void spim() {
		List<Integer> tmp = new ArrayList<Integer>();
		for (int i = 0; i < lines.size() - 1; i++) {
			if (lines.get(i).startsWith("l") && lines.get(i + 1).startsWith("s")) {
				if (lines.get(i).substring(1).equals(lines.get(i + 1).substring(1))) {
					tmp.add(i);
					tmp.add(i + 1);
					if (DEBUG >= 4)
							System.err.println(lines.get(i) + "\t" + lines.get(i + 1)) ;
					i++;
				}
			} else if (lines.get(i).startsWith("s") && lines.get(i + 1).startsWith("l")) {
				if (lines.get(i).substring(1).equals(lines.get(i + 1).substring(1))) {
					tmp.add(i + 1);
					if (DEBUG >= 4)
						System.err.println(lines.get(i) + "\t" + lines.get(i + 1)) ;
					i++;
				}
			} else if (lines.get(i).startsWith("b")) {
				String[] t = lines.get(i).split(" ");
				if (lines.get(i + 1).startsWith(t[t.length - 1])) {
					tmp.add(i);
					if (DEBUG >= 4)
						System.err.println(lines.get(i));
				}
			}
		}
		for (int i = tmp.size() - 1; i >= 0; i--)	lines.remove((int)tmp.get(i));
	}

	private void update(Set<Variable>s, Object t) {
		if (t instanceof Variable)
			s.add((Variable)t);
	}
	
	private void ref(Set<Variable>s, Object t) {
		while (t instanceof Variable) {
			update(s, t);
			t = ((Variable)t).addr;
		}
	}
	
	private void addedge(int u, int v) {
		if (v < N && u >= 0) {
			succ[u].add(v);
			pred[v].add(u);
		}
	}
	
	private void bfs(int x) {
		int h = 0, t = 1;
		queue[h] = x;
		ord.add(x);
		mark[x] = true;
		while (h < t) {
			int u = queue[h++];
			for (Integer v : pred[u])
				if (!mark[v]) {
					mark[v] = true;
					queue[t++] = v;
					ord.add(v);
					bfs(v);
				}
		}
	}
	
	private void scan() {
		cs = new ICode[codes.size()];
		N = 0;
		for (ICode c : codes)
			cs[N++] = c;
		use = new HashSet[N];
		def = new HashSet[N];
		in = new HashSet[N];
		out = new HashSet[N];
		pred = new HashSet[N];
		succ = new HashSet[N];
		for (int i = 0; i < N; i++) {
			cs[i].id = i;
			use[i] = new HashSet<Variable>();
			def[i] = new HashSet<Variable>();
			in[i] = new HashSet<Variable>();
			out[i] = new HashSet<Variable>();
			pred[i] = new HashSet<Integer>();
			succ[i] = new HashSet<Integer>();
		}
		for (ICode c : cs) {
			int i = c.id;
			if (c instanceof Label) {
				addedge(c.id, c.id + 1);
				if (c instanceof Proc) {
					if (((Proc)c).decl != null)
						for (Variable v : ((Proc)c).decl)
							update(def[i], v);
				}
			} else if (c instanceof Quad) {
				addedge(c.id, c.id + 1);
				update(def[i], ((Quad) c).A);
				ref(use[i], ((Variable)((Quad)c).A).addr);
				ref(use[i], ((Quad) c).B);
				ref(use[i], ((Quad) c).C);
			} else if (c instanceof Jump) {
				addedge(c.id, ((Jump) c).t.id);
				if (((Jump) c).op != null) {
					addedge(c.id, c.id + 1);
				}
				ref(use[i], ((Jump) c).A);
				ref(use[i], ((Jump) c).B);
			} else if (c instanceof Call) {
				String func = ((Call) c).addr.name;
				if (!func.equals("printf") && !func.equals("malloc")) {
					addedge(c.id, ((Call) c).addr.id);
					addedge(Label.get("_" + func).id, c.id + 1);
				} else
					addedge(c.id, c.id + 1);
				if (((Call) c).args != null)
					for (Term t : ((Call) c).args) {
						ref(use[i], t);
					}
			}
		}
		ord = new ArrayList<Integer>();
		queue = new int[N];
		mark = new boolean[N];
		for (int i = 0; i < N; i++)
			mark[i] = false;
		for (int i = N - 1; i >= 0; i--)
			if (!mark[i])
				bfs(i);
		
		boolean modified = true;
		int cnt = 0;
		Set<Variable> In = new HashSet<Variable>(), Out = new HashSet<Variable>();
		while (modified) {
			cnt ++;
			modified = false;
			for (Integer n : ord) {
				In.clear(); In.addAll(in[n]);
				Out.clear(); Out.addAll(out[n]);
				
				out[n].removeAll(def[n]);
				in[n] = use[n];
				in[n].addAll(out[n]);
				
				out[n].clear();
				for (Integer s : succ[n])
					out[n].addAll(in[s]);
				
				if (!modified) {
					if (!In.equals(in[n]) || !Out.equals(out[n]))
						modified = true;
				}
			}
		}
		if (DEBUG >= 2)
			System.err.println("LOOP ROUND: " + cnt);
		for (int n = 0; n < N; n++) {
			for (Variable v : out[n])
				v.begin = Math.min(v.begin, n);
			for (Variable v : in[n])
				v.end = Math.max(v.end, n);
		}
		for (ICode c : codes) {
			if (DEBUG >= 3)
				System.err.println(c.id + "\t" + c + "\t" + in[c.id] + "\t" + out[c.id] + "\t" + use[c.id] + "\t" + def[c.id]);
		}
		Collections.sort(Variable.vars, new Cmp());
		vars = new ArrayList<Variable>();
		for (Variable v : Variable.vars) {
			if (v.end == -1) {
				break;
			}
			vars.add(v);
		}
		for (int i = 0; i < cs.length; i++) {
			if (cs[i] instanceof Call && !((Call)cs[i]).addr.name.equals("printf") && !((Call)cs[i]).addr.name.equals("malloc")) {
				ArrayList<Variable> tmp = new ArrayList<Variable>();
				for (Variable v : vars)
					if ((v.begin < i && v.end > i))
						tmp.add(v);
				vars.removeAll(tmp);
			} else if (cs[i] instanceof Quad) {
				if (((Quad)cs[i]).op.optype == OpType.ADDR) {
					vars.remove(((Quad)cs[i]).B);
				} else if (((Quad)cs[i]).op.optype == OpType.MEMOF) {
					vars.remove(((Quad)cs[i]).A);
				}
			}
		}
		ArrayList<Variable> tmp = new ArrayList<Variable>();
		for (Variable v : vars)
			if (v.addr instanceof String) {
				tmp.add(v);
			}
		vars.removeAll(tmp);
		for (Variable v : vars) {
			if (DEBUG >= 2)
				System.err.println(v + "\t" + v.begin + "\t" + v.end);
		}
	}

	private void gen_data() {
		put(".data");
		put(".newline: .asciiz \"\\n\"");
		put(".align 2");
		for (RecordField r:global_vars) {
			if (r.type instanceof Pointer) {
				Object data = r.val();
				if (data instanceof String) {
					if (!(r.type instanceof Array))
						r.type = new Array(new Char(), data.toString().length() + 1);
					put(".%s: .asciiz \"%s\"", r.symbol, ((String) data).replaceAll("\n", "\\\\" + "n"));
					put(".align 2");
				} else if (data instanceof List){
					put(".%s:", r.symbol);
					for (Object t : (List)data) {
						if (t instanceof Character) {
							put(".byte %d", (int)((Character)t).charValue());
						} else if (t instanceof Integer) {
							put(".word %d", t);
						} else {
							put(".space %d", r.symbol, RecordField.align(((Array)r.type).elementType.size()));
						}
					}
					put(".align 2");
				} else {
					put(".%s: .space %d", r.symbol, RecordField.align(r.type.size()));
				}
			} else if (r.type.size() == 4) {
				put(".%s: .word %d", r.symbol, r.val());
			} else if (r.type instanceof Char) {
				put(".%s: .ascii \"%c\"", r.symbol, r.val());
				put(".align 2");
			} else {
				put(".extern .%s %d", r.symbol, RecordField.align(r.type.size()));
			}
		}
		put("");
	}

	private void gen_text() {
		put(".text");
		if (DEBUG >= 2) {
			include("debug.s");
		}
		include("malloc.s");
		include("printf.s");
		int i = 0;
		active = new LinkedList<Variable>();
		for (Variable v : vars) {
			String reg;
			while (i < v.begin || cs[i] instanceof Proc)
				gen(cs[i++]);
			
			List<Variable> tmp = new LinkedList<Variable>();
			for (Variable a : active)
				if (a.end < v.begin) {
					reg = a.reg;
					a.reg = null;
					if (a.addr != null)
						save(reg, a);
					free(reg);
					tmp.add(a);
				}
			active.removeAll(tmp);
			
			reg = use();
			if (reg == null) {
				Variable t = active.get(active.size() - 1);
				if (t.end > v.end) {
					reg = t.reg;
					t.reg = null;
					save(reg, t);
					load(reg, v, true);
					v.reg = reg;
					active.remove(active.size() - 1);
					active.add(v);
				}
			} else {
				if (v.decl.pos.equals("$fp")) {
					load(reg, v, true);
				}
				v.reg = reg;
				active.add(v);
			}
		}
		while (i < cs.length)
			gen(cs[i++]);
	}
	
	private void gen(ICode i) {
		if (i instanceof Label) {
			gen((Label)i);
		} else if (i instanceof Call) {
			gen((Call)i);
		} else if (i instanceof Quad) {
			gen((Quad)i);
		} else if (i instanceof Jump) {
			gen((Jump)i);
		}
	}
	
	private void gen(Label c) {
		if (DEBUG >= 1)
			put("nop #%s", c.name);
		putl(c.name);
		if (c instanceof Proc) {
			if (!c.name.startsWith("_")) {
				puti("sw", fp, sp, -4);
				puti("sw", ra, sp, -8);
				puti("move", fp, sp);
				puti("addiu", sp, sp, -((Proc) c).stack_size);
			} else {
				puti("addiu", sp, sp, ((Proc) c).stack_size);
				puti("lw", fp, sp, -4);
				puti("lw", ra, sp, -8);
				puti("jr", ra);
				put("nop");
				put("");
			}
		}
	}
	
	private void gen(Call c) {
		if (c.addr.name.equals("printf")) {
			if (c.args.size() == 1) {
				puti("li", v0, 4);
				Term v = c.args.get(0);
				String reg = get(a0, v, false);
				if (reg != a0)
					puti("move", a0, reg);
				put("syscall");
				return ;
			}
		}
		if (c.addr.name.equals("malloc")) {
			puti("li", v0, 9);
			Term v = c.args.get(0);
			String reg = get(a0, v, true);
			if (reg != a0)
				puti("move", a0, reg);
			put("syscall");
			return ;
		}
		if (c.args != null) {
			int offset = 0;
			for (Term v : c.args) {
				offset += move(v, sp, offset);
			}
		}
		puti("jal", c.addr.name);
	}
	
	private void gen(Quad c) {
		if (!out[c.id].containsAll(def[c.id])) {
//			System.err.println(c + " " + out[c.id] + " " + def[c.id]);
//			return ;
		}
		debug(c.toString());
		if (c.op.optype == OpType.MEMOF) {
			
		} else if (c.op.optype == OpType.ASSIGN) {
			if (((Variable)c.A).reg != null) {
//				load(((Variable)c.A).reg, c.B, c._B);
//				return ;
			}
		}
		String rs, rt, rd;
		rs = get(a0, c.B, c._B);
		rd = ((Variable)c.A).reg != null ? ((Variable)c.A).reg : a0;
		if (c.C != null) {
			if (c.op.optype == OpType.COMMA || c.op.optype == OpType.CAST) {
				rt = get(rd, c.C, c._C);
			} else if (c.op.optype == OpType.DIVIDE) {
				rt = get(a1, c.C, c._C);
				puti("div", rs, rt);
				puti("mflo", rd);
			} else if (c.op.optype == OpType.MODULO) {
				rt = get(a1, c.C, c._C);
				puti("div", rs, rt);
				puti("mfhi", rd);
			} else {
				if (c.C instanceof Scalar && c.op.asmri() != "") {
					int k = (((Scalar)c.C).value instanceof Integer ? ((Integer)((Scalar)c.C).value) : ((Character)((Scalar)c.C).value).charValue());
					puti(c.op.asmrr(), rd, rs, k);
				} else {
					rt = get(a1, c.C, c._C);
					puti(c.op.asmrr(), rd, rs, rt);
				}
			}
		} else {
			if (c.op.optype == OpType.NOT) {
				puti("seq", rd, rs, $0);
			} else if (c.op.optype == OpType.ASSIGN) {
				if (((Variable)c.A).decl.type instanceof Record) {
					rt = get(a1, c.A, false);
					memcpy(rs, rt, ((Variable)c.A).decl.type.size());
					return ;
				} else {
					rd = rs;
				}
			} else if (c.op.optype == OpType.MEMOF) {
				String ins = ((Variable)c.A).decl.type instanceof Char ? "lb" : "lw";
				puti(ins, rd, rs, 0);
			} else {
				puti(c.op.asmr(), rd, rs);
			}
		}
		save(rd, (Variable)c.A);
	}

	private void gen(Jump c) {
		debug(c.toString());
		String rs, rt;
		if (c.op == null) {
			puti("b", c.t);
		} else if (c.op.optype == OpType.COMMA) {
			rs = get(a0, c.B, true);
			puti("bne", rs, $0, c.t.name);
		} else if (c.op.optype == OpType.CAST) {
			rs = get(a0, c.B, true);
			puti("bne", rs, $0, c.t.name);
		} else if (c.op.optype == OpType.DIVIDE) {
			rs = get(a0, c.A, true);
			rt = get(a1, c.B, true);
			puti("div", rs, rt);
			puti("mflo", a0);
			puti("bne", a0, $0, c.t.name);
		} else if (c.op.optype == OpType.MODULO) {
			rs = get(a0, c.A, true);
			rt = get(a1, c.B, true);
			puti("div", rs, rt);
			puti("mfhi", a0);
			puti("bne", a0, $0, c.t.name);
		}	else {
			rs = get(a0, c.A, true);
			String o = c.op.toString().toLowerCase();
			if (c.op.isCompare()) {
				if (c.B != null) {
					rt = get(a1, c.B, true);
					put("b%s %s %s %s", o, rs, rt, c.t.name);
				} else {
					put("b%s %s %s %s", o, rs, $0, c.t.name);
				}
			} else {
				if (c.B != null) {
					rt = get(a1, c.B, true);
					puti(c.op.asmrr(), a0, rt, rs);
					puti("bne", a0, $0, c.t.name);
				} else if (c.op.optype == OpType.NOT) {
					puti("beq", rs, $0, c.t.name);
				} else throw new Error("TODO JUMP");
			}
		}
	}
	
	private String get(String rd, Term v, boolean is_value) {
		if (v instanceof Scalar) {
			if (((Scalar)v).value instanceof Integer)
				if ((Integer)((Scalar)v).value == 0)
					return $0;
			load(rd, (Scalar)v);
		}
		else if (v instanceof Variable) {
			if (is_value && ((Variable) v).reg != null)
				return ((Variable) v).reg;
			load(rd, (Variable)v, is_value);
		}
		return rd;
	}
	
	private void load(String rd, Term v, boolean is_value) {
		if (v instanceof Scalar)
			get(rd, (Scalar)v, is_value);
		else
			load(rd, (Variable)v, is_value);
	}
	
	private void load(String rd, Variable v, boolean is_value) { // only load word
		String ins = v.decl.type instanceof Char ? "lb" : "lw";
		if (is_value) {
			if (v.reg != null) {
				puti("move", rd, v.reg);
			} else if (v.addr instanceof String) {
				puti(ins, rd, v.addr);
			} else if (v.addr instanceof Term) {
				puti(ins, rd, get(rd, (Term) v.addr, true), 0);
			} else if (v.decl != null) {
				// Local Variable
				puti(ins, rd, v.decl.pos, v.decl.offset);
			}
		} else {
			if (v.addr instanceof String) {
				puti("la", rd, v.addr);
			} else if (v.addr instanceof Term) {
				load(rd, (Variable) v.addr, true);
			} else if (v.decl != null) {
				puti("la", rd, v.decl.pos, v.decl.offset);
			}
		}
	}
	
	private void load(String rd, Scalar v) {
		if (v.value instanceof Integer) {
			puti("li", rd, (Integer)v.value);
		} else if (v.value instanceof Character) {
			puti("la", rd, "'" + v.value.toString() + "'");
		}
	}
	
	private void save(String rd, Variable v) { // only save word
		if (v.reg != null) {
			if (v.reg != rd)
				puti("move", v.reg, rd);
			return ;
		}
		String ins = v.decl.type instanceof Char ? "sb" : "sw";
		if (v.addr != null) {
			String reg = use();
			if (v.addr instanceof Variable) {
				if (((Variable)v.addr).reg != null) {
					puti(ins, rd, ((Variable)v.addr).reg, 0);
					free(reg);
					return ;
				}
				load(reg, (Variable) v.addr, true);
			} else if (v.addr instanceof String) {
				puti("la", reg, v.addr);
			}
			puti(ins, rd, reg, 0);
			free(reg);
			return ;
		}
		puti(ins, rd, v.decl.pos, v.decl.offset);
	}
	
	private int move(Term v, String rt, Integer offset) {
		if (v instanceof Scalar) {
			load(a2, (Scalar)v);
			puti("sw", a2, rt, offset);
			return 4;
		} else if (v instanceof Variable) {
			if (((Variable) v).decl.type instanceof Array) {
				String reg = a2;
				String rs = get(reg, v, false);
				puti("sw", rs, rt, offset);
				return 4;
			}
			if (((Variable) v).decl.type.size() <= 4) {
				String reg = a2;
				String rs = get(reg, v, true);
				puti("sw", rs, rt, offset);
				return 4;
			}
			int size = RecordField.align(((Variable)v).decl.type.size());
			for (int now = 0; now < size; now += 4) {
				puti("lw", a2, ((Variable) v).decl.pos, ((Variable) v).decl.offset + now);
				puti("sw", a2, rt, offset + now);
			}
			return size;
		}
		return 0;
	}
	
	private void memcpy(String rs, String rt, Integer size) {
		size = RecordField.align(size);
		String reg = a2;
		for (int offset = 0; offset < size; offset += 4) {
			puti("lw", reg, rs, offset);
			puti("sw", reg, rt, offset);
		}
	}

}
