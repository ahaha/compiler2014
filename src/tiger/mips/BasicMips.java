package tiger.mips;

import java.io.BufferedReader;
import java.io.FileReader;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import tiger.translate.ICode;
import tiger.type.RecordField;

public class BasicMips {
	protected int DEBUG;
	protected List<String> lines;
	protected ArrayList<ICode> codes;
	protected List<RecordField> global_vars;
	protected List<String> regs;
	protected List<String> usable_regs, lock;

	public String v0, v1;
	public String at, $0, k0, k1, sp, gp, fp, ra;
	public String a0, a1, a2, a3;
	public String s0, s1, s2, s3, s4, s5, s6, s7;
	public String t0, t1, t2, t3, t4, t5, t6, t7, t8, t9;

	protected void puti(String ins, String rd, Object rs) {
		if (ins.equals("sw")
				|| ins.equals("sb") // Store Instruction
				|| ins.equals("lw") || ins.equals("lb") || ins.equals("li")
				|| ins.equals("la")) { // Load Instruction
			if (rs.toString().startsWith("$")) {
				puti(ins, rd, rs.toString(), 0);
				return;
			}
		}
		put("%s %s %s", ins, rd, rs.toString());
	}

	protected void puti(String ins, Object rd) {
		put("%s %s", ins, rd.toString());
	}

	protected void putl(String lab) {
		put("%s:", lab);
	}

	protected void puti(String ins, String rd, String rs, Object rt) {
		if (ins.equals("sw")
				|| ins.equals("sb") // Store Instruction
				|| ins.equals("lw") || ins.equals("lb") || ins.equals("li")
				|| ins.equals("la")) { // Load Instruction
			if (Math.abs(((Integer) rt)) > 32767) {
				String reg = use();
				put("li %s %d", reg, rt);
				put("addu %s %s %s", reg, rs, reg);
				put("%s %s 0(%s)", ins, rd, reg);
				free(reg);
			} else {
				put("%s %s %d(%s)", ins, rd, rt, rs);
			}
		} else if (rt instanceof Integer) {
			if (Math.abs((Integer) rt) > 32767) {
				if (rs.equals(rd)) {
					String reg = use();
					put("li %s %d", reg, rt);
					put("%s %s %s %s", ins.replace("i", ""), rd, rs, reg);
					free(reg);
				} else {
					put("li %s %d", rd, rt);
					put("%s %s %s %s", ins.replace("i", ""), rd, rs, rd);
				}
			} else {
				put("%s %s %s %d", ins, rd, rs, rt);
			}
		} else if (rt instanceof String) {
			put("%s %s %s %s", ins, rd, rs, rt);
		}
	}

	protected void lock(String reg) {
		lock.add(reg);
		usable_regs.remove(reg);
	}

	protected String use() {
		if (usable_regs.size() == 0) {
			return null;
//			throw new Error("No available register");
		}
		String reg = usable_regs.get(0);
		lock(reg);
		return reg;
	}

	protected void free(String reg) {
		if (lock.contains(reg)) {
			lock.remove(reg);
		}
		if (!usable_regs.contains(reg)) {
			usable_regs.add(reg);
		}
	}

	protected void debug(String comment) {
		if (DEBUG < 0)
			put("jal .debug \t\t\t#%s", comment);
		else if (DEBUG >= 2)
			put("nop \t\t\t#%s", comment);
	}

	protected void include(String filename) {
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(filename));
			String line = br.readLine();
			while (line != null) {
				lines.add(line);
				line = br.readLine();
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void put(String fmt, Object... objects) {
		lines.add(String.format(fmt, objects));
	}

	public String toString() {
		StringBuffer sb = new StringBuffer();
		for (String line : lines) {
			if (!line.startsWith(".") && !line.contains(":")
					&& !line.startsWith("#") && !line.startsWith("\t")) {
				sb.append("\t");
			}
			sb.append(line);
			sb.append("\n");
		}
		return sb.toString();
	}

	public BasicMips(ArrayList<ICode> c, List<RecordField> g, int d)
			throws Exception {
		codes = c;
		global_vars = g;
		DEBUG = d;
		lines = new LinkedList<String>();
		regs = new ArrayList<String>();
		lock = new ArrayList<String>();
		usable_regs = new ArrayList<String>();

		for (Field f : this.getClass().getFields()) {
			String name = f.getName();
			f.set(this, name.startsWith("$") ? name : "$" + name);
			if ((name.startsWith("a") || name.startsWith("s")
					|| name.startsWith("v") || name.startsWith("t"))) {
				usable_regs.add((String) f.get(this));
			}
		}
		regs.addAll(usable_regs);
		lock(a0);
		lock(a1);
		lock(a2);
		lock(at);
		lock(v0);
		if (DEBUG >= 2) {
			lock(t9);
			lock(v1);
		}
		lock(sp);
	}
}
