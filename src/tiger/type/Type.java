package tiger.type;

public abstract class Type {
	public String toString() {
		return "Undefined";
	}
	
	public int size() {
		return 4;
	}
	
	/*
	 * check if we can cast a to b
	 */
	public static boolean cast(Type a, Type b) {
		if (a instanceof Basic && b instanceof Basic)
			return true;
		if (a instanceof Pointer && b instanceof Pointer)
			return true;
		if (a instanceof Pointer && b instanceof Basic)
			return true;
		if (b instanceof Pointer && a instanceof Basic)
			return true;
		if (a.equals(b))
			return true;
		return false;
	}
	
	public static Type arr2ptr(Type t) {
		if (!(t instanceof Pointer))
			return t;
		return new Pointer(arr2ptr(((Pointer)t).elementType));
	}
}
