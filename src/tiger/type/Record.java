package tiger.type;

import java.util.LinkedList;
import java.util.List;

import tiger.symbol.Symbol;

public abstract class Record extends Type {

	public Symbol symbol;
	public int sz;
    public List<RecordField> fields;
    
    public Record() {
    	fields = new LinkedList<RecordField>();
    }
    
    public boolean equals(Object o) {
    	return this == o;
    }
    public int size() {
    	return sz;
    }
}
