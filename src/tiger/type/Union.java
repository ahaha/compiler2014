package tiger.type;

public final class Union extends Record {
	public String toString() {
		String tmp = "Union { ";
		for (RecordField r: fields)
			tmp += r.type + " ";
		tmp += " }";
		return tmp;
	}
}
