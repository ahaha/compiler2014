package tiger.type;

public final class Struct extends Record {
	public String toString() {
		String tmp = "Struct { ";
		for (RecordField r: fields)
			tmp += r.type + " ";
		tmp += " }";
		return tmp;
	}
}
