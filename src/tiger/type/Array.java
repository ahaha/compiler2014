package tiger.type;

public final class Array extends Pointer {
    public int capacity;
    
    public Array(Type t, int c) {
    	super(t);
    	capacity = c;
    }
    
    public String toString() {
		return elementType + "[" + capacity + "]";
    }
    
    public boolean equals(Object o) {
    	if (o instanceof Array) {
    		return elementType.equals(((Array) o).elementType);
    	}
    	return false;
    }
    
    public int size() {
    	int t = capacity * elementType.size();
    	return t;
    }
}
