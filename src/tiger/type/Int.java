package tiger.type;

public final class Int extends Type implements Basic {
	public String toString() {
		return "Int";
	}
	
	public boolean equals(Object o) {
		return o instanceof Int;
	}
	
}
