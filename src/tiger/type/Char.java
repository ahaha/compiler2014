package tiger.type;

public final class Char extends Type implements Basic {
	public String toString() {
		return "Char";
	}
	
	public boolean equals(Object o) {
		return o instanceof Char;
	}
	
	public int size() {
		return 1;
	}
}
