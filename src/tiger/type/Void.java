package tiger.type;

public final class Void extends Type {
	public String toString() {
		return "Void";
	}
	
	public boolean equals(Object o) {
		return o instanceof Void;
	}
}
