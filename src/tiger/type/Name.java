package tiger.type;

public final class Name extends Type {
    public String name;
    public Name(String n) {
    	name = n;
    }
    public String toString() {
    	return "Name(" + name + ")";
    }
    public boolean equals(Object o) {
    	if (o instanceof Name) {
    		return name.equals(((Name) o).name);
    	}
    	return false;
    }
}
