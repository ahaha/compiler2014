package tiger.type;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import tiger.ast.*;
import tiger.symbol.Symbol;
import tiger.translate.Scalar;

public class RecordField {
	public Symbol symbol;
	public Type type;
	public int offset;
	public Object init;
	public String pos;
	public static int align(int k) {
		return k % 4 == 0 ? k : k + 4 - k %  4;
	}
	private Object val(Initializer init) {
		if (init == null)
			return 0;
		if (init instanceof ExprInitializer)
			return ((ExprInitializer) init).expr.val;
		if (init instanceof ArrayInitializer) {
			List<Object> list = new ArrayList<Object>();
			for (Initializer i : ((ArrayInitializer) init).list)
				list.add(val(i));
			return list;
		}
		return 0;
	}
	public Object val() {
		if (init instanceof Initializer)
			return val((Initializer) init);
		else if (init == null)
			return 0;
		else
			return init;
	}
	public RecordField(Symbol s, Type t) {
		symbol = s;
		type = t;
		offset = 0;
		init = null;
		pos = "$sp";
	}
	public RecordField(Type t) {
		type = t;
		offset = 0;
		init = null;
		pos = "$sp";
	}
	public RecordField(Type t, AtomicInteger o) {
		type = t;
		pos = "$sp";
		offset = o.get();
		if (t instanceof Pointer)
			o.addAndGet(4);
		else
			o.addAndGet(align(t.size()));
	}
}
