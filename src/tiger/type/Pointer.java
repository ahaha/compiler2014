package tiger.type;

import tiger.ast.PlainDeclarator;

public class Pointer extends Type {
	public Type elementType;
	
	public Pointer(Type t) {
		elementType = t;
	}
	
	public Pointer(Type t, PlainDeclarator d) {
		if (d.declarator == null) {
			elementType = t;
		} else {
			elementType = new Pointer(t, (PlainDeclarator) d.declarator);
		}
	}
	
	public String toString() {
		return "*" + elementType;
	}
	
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o instanceof Pointer) {
			return elementType.equals(((Pointer) o).elementType);
		}
		return false;
	}
}
