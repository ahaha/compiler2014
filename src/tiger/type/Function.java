package tiger.type;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public final class Function extends Type {
    public List<Type> argumentType;
    public Type returnType;
    public boolean dynamic;
    
    public Function(Type r, List<Type> a) {
    	argumentType = a;
    	returnType = r;
    }
    
    public Function(Type r) {
    	returnType = r;
    	argumentType = new LinkedList<Type>();
    }

    public Function(Type r, boolean d) {
    	returnType = r;
    	dynamic = d;
    	argumentType = new LinkedList<Type>();
    }    
    
    public String toString() {
		String tmp = returnType + "( ";
		for (Type arg: argumentType)
			tmp += arg + " ";
		tmp += ")";
		return tmp;
    }
    
    public boolean equals(Object o) {
    	if (o instanceof Function) {
    		if (returnType.equals(((Function) o).returnType)) {
    			if (dynamic)
    				return true;
    			if (argumentType.size() != ((Function)o).argumentType.size())
    				return false;
    			Iterator<Type> it = ((Function) o).argumentType.iterator();
    			for (Type t : argumentType) {
    				if (!Type.cast(it.next(), t))
    					return false;
    			}
    			return true;
//    			return dynamic || argumentType.equals(((Function) o).argumentType);
    		}
    	}
    	return false;
    }
    
}
