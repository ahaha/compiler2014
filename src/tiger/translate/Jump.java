package tiger.translate;

import tiger.ast.Op;

public class Jump extends Operate {
	public Label t;
	public Op op;
	public Term A, B;
	public Jump(Label v) {
		t = v;
	}
	public Jump(Label v, Op o, Term a) {
		t = v;
		op = o;
		A = a;
	}
	public Jump(Label v, Op o, Term a, Term b) {
		t = v;
		op = o;
		A = a;
		B = b;
	}
	public String toString() {
		if (B != null)
			return "\tJump " + t + " If " + A + " " + op + " " + B;
		else if (A != null)
			return "\tJump " + t + " If " + op + " " + A;
		else
			return "\tJump " + t;
	}
}
