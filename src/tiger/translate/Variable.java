package tiger.translate;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import tiger.type.*;

public class Variable extends Term {
	private static int count = 0;
	public int begin, end;
	public RecordField decl;
	public Integer value;
	public Object addr;
	public String reg;
	public String name;
	static public List<Variable> vars = new ArrayList<Variable>();
	public Variable() {
		begin = 123456789;
		end = -1;
		name = "V_" + Integer.toString(++count);
		vars.add(this);
	}
	public Variable(String n) {
		begin = 123456789;
		end = -1;
		name = n;// + "_" + Integer.toString(++count);
		vars.add(this);
	}
	public String toString() { return name; }
}
