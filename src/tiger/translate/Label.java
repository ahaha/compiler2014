package tiger.translate;

import java.util.Hashtable;

public class Label extends Term {
	private static int count = 0;
	private static Hashtable<String, Label> labels = new Hashtable<String, Label>();
	public String name;
	public Label() {
		name = "L_" + Integer.toString(++count);
		labels.put(name, this);
	}
	public Label(String n) {
		name = n;
		labels.put(name, this);
	}
	public String toString() {
		return name;
	}
	public static Label get(String n) {
		return labels.get(n);
	}
}
