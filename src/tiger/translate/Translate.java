package tiger.translate;

import static tiger.symbol.Symbol.symbol;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;
import java.util.concurrent.atomic.AtomicInteger;

import tiger.ast.*;
import tiger.ast.Op.OpType;
import tiger.symbol.Symbol;
import tiger.symbol.Table;
import tiger.type.*;
import tiger.type.Void;

public class Translate {
	Program prog;
	Stack<AtomicInteger> local_vars;
	Stack<Label> loop_restart, loop_out, func_ret;
	public static Variable RetVal;
	public Variable Zero;
	AtomicInteger gp_offset, fp_offset, sp_offset;
	Table vscope;
	public ArrayList<ICode> codes;
	public List<RecordField> global_vars;

	public Translate(Program program) {
		prog = program;
		codes = new ArrayList<ICode>();
		local_vars = new Stack<AtomicInteger>();
		loop_restart = new Stack<Label>();
		loop_out = new Stack<Label>();
		func_ret = new Stack<Label>();
		RetVal = new Variable("RetVal");
		RetVal.addr = "$v0";
		RetVal.reg = "$v0";
		RetVal.decl = new RecordField(new Symbol("RetVal"), new Int());
		RetVal.decl.pos = "";
		Zero = new Variable("Zero");
		Zero.addr = "$0";
		Zero.reg = "$0";
		Zero.decl = new RecordField(new Symbol("Zero"), new Int());
		Zero.decl.pos = "";
		gp_offset = new AtomicInteger(8000);
		fp_offset = new AtomicInteger(0);
		sp_offset = new AtomicInteger(0);
		global_vars = new LinkedList<RecordField>();
		vscope = new Table();
		vscope.put(symbol("printf"), new Proc("printf"));
		vscope.put(symbol("malloc"), new Proc("malloc"));
		trans(prog);
	}
	
	private void trans(Program prog) {
		for (Def p: prog.list) {
			if (p instanceof Decl) {
				trans((Decl)p, gp_offset);
			} else if (p instanceof Func) {
				trans((Func)p);
			}
		}
	}
	
	private void trans(Decl d, AtomicInteger offset) {
		int off = offset.get();
		for (RecordField r: d.list) {
			if (offset == gp_offset) {
				r.pos = "$gp";
				global_vars.add(r);
				r.offset = off;
				off += RecordField.align(r.type.size());
			} else {
				r.pos = "$fp";
				off -= RecordField.align(r.type.size());
				r.offset = off;
			}
			vscope.put(r.symbol, r);
			if (r.init != null) {
				if (!r.pos.equals("$gp")) {
					Term v = trans((Initializer)r.init);
					codes.add(new Quad(new Op(OpType.ASSIGN), trans(r.symbol), v, isvalue(v)));
				}
			}
		}
		offset.set(off);
	}
	
	private void trans(Func f) {
		Proc Start = new Proc(f.declarator.id.sym.toString());
		Proc Final = new Proc("_" + f.declarator.id.sym.toString());
		codes.add(Start);
		func_ret.push(Final);
		local_vars.push(new AtomicInteger(4 * 12));
		
		vscope.put(f.declarator.id.sym, f);
		vscope.beginScope();
		int arg_size = 0;
		List<Variable> arg_decls = new LinkedList<Variable>();
		for (RecordField r:f.args) {
			r.offset = arg_size;
			r.pos = "$fp";
			r.type = Type.arr2ptr(r.type);
//			arg_size += r.type instanceof Array ? 4 : RecordField.align(r.type.size());
			arg_size += RecordField.align(r.type.size());
			vscope.put(r.symbol, r);
			arg_decls.add((Variable) trans(r.symbol));
		}
		Start.decl = arg_decls;
		
		fp_offset.set(-8);
		if (f.stmt.decls != null) {
			for (Decl d: f.stmt.decls.list) {
				trans(d, fp_offset);
			}
		}
		
		sp_offset.set(0);
 		
		if (f.stmt.stmts != null) {
			for (Stmt s: f.stmt.stmts.list) {
				trans(s);
			}
		}
		vscope.endScope();
		codes.add(Final);
		Start.temp_size = local_vars.pop().get();
		Start.newarg_size = Start.temp_size + sp_offset.get();
		Start.stack_size = Start.newarg_size - fp_offset.get();
		Final.stack_size = Start.stack_size;
//		System.err.println(Start + " " + Start.stack_size);
		func_ret.pop();
	}

	private void trans(Stmt s) {
		if (s instanceof Expr) {
			trans((Expr)s);
		} else if (s instanceof CompoundStmt) {
			trans((CompoundStmt)s);
		} else if (s instanceof SelectionStmt) {
			trans((SelectionStmt)s);
		} else if (s instanceof IterationStmt) {
			trans((IterationStmt)s);
		} else if (s instanceof JumpStmt) {
			trans((JumpStmt)s);
		}
	}	
	
	private void trans(CompoundStmt stmt) {
		vscope.beginScope();
		if (stmt.decls != null) {
			for (Decl d: stmt.decls.list) {
				trans(d, fp_offset);
			}
		}
		if (stmt.stmts != null) {
			for (Stmt s: stmt.stmts.list)
				trans(s);
		}
		vscope.endScope();
	}
	
	private void trans(SelectionStmt stmt) {
		if (stmt.elseStmt != null) {
			Label Else = new Label();
			Label End = new Label();
			Label Then = new Label();
			trans(stmt.ifExpr, Then, Else);
			codes.add(Then);
			trans(stmt.thenStmt);
			codes.add(new Jump(End));
			codes.add(Else);
			trans(stmt.elseStmt);
			codes.add(End);
		} else {
			Label End = new Label();
			Label Then = new Label();
			trans(stmt.ifExpr, Then, End);
			codes.add(Then);
			trans(stmt.thenStmt);
			codes.add(End);
		}
	}
	
	private void trans(IterationStmt stmt) {
		Label Init = new Label();
		Label Test = new Label();
		Label Out = new Label();
		Label Continue = new Label();
		loop_restart.push(Continue);
		loop_out.push(Out);
		if (stmt instanceof ForStmt) {
			if (((ForStmt) stmt).initExpr != null)
					trans(((ForStmt) stmt).initExpr);
			codes.add(new Jump(Test));
			codes.add(Init);
			if (((ForStmt) stmt).stmt != null) {
				trans(((ForStmt) stmt).stmt);
			}
			codes.add(Continue);
			if (((ForStmt) stmt).stepExpr != null)
				codes.add(trans(((ForStmt) stmt).stepExpr));
			codes.add(Test);
			trans(((ForStmt) stmt).testExpr, Init, Out);
		} else if (stmt instanceof WhileStmt) {
			codes.add(new Jump(Test));
			codes.add(Init);
			if (((WhileStmt) stmt).stmt != null) {
				trans(((WhileStmt) stmt).stmt);
			}
			codes.add(Continue);
			codes.add(Test);
			trans(((WhileStmt) stmt).testExpr, Init, Out);
		}
		codes.add(Out);
		loop_restart.pop();
		loop_out.pop();
	}
	
	private void trans(JumpStmt stmt) {
		if (stmt instanceof BreakStmt) {
			codes.add(new Jump(loop_out.lastElement()));
		} else if (stmt instanceof ContinueStmt) {
			codes.add(new Jump(loop_restart.lastElement()));
		} else if (stmt instanceof ReturnStmt) {
			Term v = trans(((ReturnStmt) stmt).retExpr);
			codes.add(new Quad(new Op(OpType.ASSIGN), RetVal, v, isvalue(v)));
			codes.add(new Jump(func_ret.lastElement()));
		}
	}
	
	private Term trans(Expr e) {
		if (e instanceof BinaryExpr) {
			return trans((BinaryExpr)e);
		} else if (e instanceof UnaryExpr) {
			return trans((UnaryExpr)e);
		} else if (e instanceof TypeSpecifier) {
		} else if (e instanceof Arg) {
			for (Expr r : ((Arg) e).args) {
				System.err.println(r.type);
			}
			System.err.println("TODO Translate Arg");
			//throw new Error("Unexpeted translation");
		} else if (e instanceof Initializer) {
			return trans((Initializer)e);
		} else if (e instanceof Var) {
			return trans((Var)e);
		} else if (e instanceof Constant) {
			return trans((Constant)e);
		}
		return null;
	}
	
	private void trans(Expr e, Label t, Label f) {
		if (e instanceof BinaryExpr) {
			if (((BinaryExpr) e).op.optype == OpType.OR) {
				trans(((BinaryExpr) e).left, t, null);
				trans(((BinaryExpr) e).right, t, f);
				return ;
			} else if (((BinaryExpr) e).op.optype == OpType.AND) {
				trans(((BinaryExpr) e).left, null, f);
				trans(((BinaryExpr) e).right, t, f);
				return ;
			}
		}
		if (e instanceof BinaryExpr) {
			if (t != null || f != null) {
				if (((BinaryExpr) e).op.asmrr() == "") {
					Variable v = new Variable();
					v.decl = new RecordField(new Int(), local_vars.lastElement());
					codes.add(new Quad(new Op(OpType.ASSIGN), v, trans(e), true));
					if (t != null)
						codes.add(new Jump(t, new Op(OpType.NE), v, Zero));
					if (f != null)
						codes.add(new Jump(f, new Op(OpType.EQ), v, Zero));
				} else {
					if (t != null)
						codes.add(new Jump(t, ((BinaryExpr) e).op, trans(((BinaryExpr) e).left), trans(((BinaryExpr) e).right)));
					if (f != null)
						codes.add(new Jump(f, ((BinaryExpr) e).op.not(), trans(((BinaryExpr) e).left), trans(((BinaryExpr) e).right)));
				}
			}
		}
		if (e instanceof UnaryExpr) {
			if (t != null || f != null) {
				if (((UnaryExpr) e).op.asmr() == "") {
					Variable v = new Variable();
					v.decl = new RecordField(new Int(), local_vars.lastElement());
					codes.add(new Quad(new Op(OpType.ASSIGN), v, trans(e), true));
					if (t != null)
						codes.add(new Jump(t, new Op(OpType.NE), v, Zero));
					if (f != null)
						codes.add(new Jump(f, new Op(OpType.EQ), v, Zero));
				} else {
					if (t != null)
						codes.add(new Jump(t, ((UnaryExpr) e).op, trans(((UnaryExpr) e).expr)));
					if (f != null)
						codes.add(new Jump(f, ((UnaryExpr) e).op.not(), trans(((UnaryExpr) e).expr)));
				}
			}
		}
		if (e instanceof Constant) {
			if (e.val.equals(0) || e.equals('\0')) {
				if (f != null)
					codes.add(new Jump(f));
			} else {
				if (t != null)
					codes.add(new Jump(t));
			}
		}
		if (e instanceof Var) {
			if (t == null && f == null)
				return ;
			Term v = trans(e);
			if (t != null)
				codes.add(new Jump(t, new Op(OpType.NE), v, Zero));
			if (f != null)
				codes.add(new Jump(f, new Op(OpType.EQ), v, Zero));
		}
	}
	
	public static boolean isvalue(Term v) {
		if (v instanceof Variable) {
			if (((Variable) v).decl.type instanceof Int)
				return true;
			if (((Variable) v).decl.type instanceof Char)
				return true;
			if (((Variable) v).decl.type instanceof Array)
				return false;
			if (((Variable) v).decl.type instanceof Pointer)
				return true;
			return false;
		}
		return true;
	}
	
	private Term trans(BinaryExpr e) {
		Term lv = trans(e.left), rv = null;
		Term v = null;
		if (e.op.isAssign()) {
			rv = trans(e.right);
			if (e.op.optype == OpType.ASSIGN) {
				codes.add(new Quad(e.op, lv, rv, isvalue(rv)));
			} else
				codes.add(new Quad(e.op.reduce(), lv, lv, isvalue(rv), rv, isvalue(rv)));
			return lv;
		} else if (e.op.optype == OpType.COMMA) {
			return trans(e.right);
		} else if (e.op.optype == OpType.CAST) {
			rv = trans(e.right);
			return rv;
			/*
			v = new Variable();
			((Variable)v).decl = new RecordField(((Variable)rv).decl.symbol, e.left.type);
			((Variable)v).decl.pos = ((Variable)rv).decl.pos;
			((Variable)v).decl.offset = ((Variable)rv).decl.offset;
			((Variable)v).decl.init = ((Variable)rv).decl.init;
			*/
		} else if (e.op.optype == OpType.BRACKET) {
			v = new Variable();
			((Variable)v).decl = new RecordField(new Pointer(((Pointer)e.left.type).elementType), local_vars.lastElement());
			rv = trans(e.right);
			if (e.left.type instanceof Pointer) {
				if (rv instanceof Scalar) {
					Integer k = (Integer) ((Scalar)rv).value * ((Pointer)e.left.type).elementType.size();
					((Scalar)rv).value = k;
					codes.add(new Quad(new Op(OpType.PLUS), v, lv, isvalue(lv), rv, true));
				} else if (rv instanceof Variable) {
					Variable tv = new Variable();
					tv.decl = new RecordField(new Int(), local_vars.lastElement());
					int k = ((Pointer)e.left.type).elementType.size();
					int p = 0;
					while (k % 2 == 0) {
						k /= 2;
						p ++;
					}
					if (k != 1) {
						codes.add(new Quad(new Op(OpType.TIMES), tv, rv, true, new Scalar(k * (1 << p)), true));
					} else {
						codes.add(new Quad(new Op(OpType.SHL), tv, rv, true, new Scalar(p), true));
					}
					codes.add(new Quad(new Op(OpType.PLUS), v, lv, isvalue(lv), tv, true));
				}
			}
			Variable nv = new Variable();
			nv.addr = v;
			nv.decl = new RecordField(((Pointer)e.left.type).elementType, local_vars.lastElement());
			codes.add(new Quad(new Op(OpType.MEMOF), nv, v, true));
			return nv;
		} else if (e.op.optype == OpType.PAREN) {
			int size = 0;
			List<Term> vv = trans((Arg)e.right);
			for (Term t : vv) {
				if (t instanceof Variable) {
					size += ((Variable)t).decl.type instanceof Array ? 4 : ((Variable)t).decl.type.size();
				} else
					size += 4;
				size = RecordField.align(size);
			}
			sp_offset.set(Math.max(sp_offset.get(), size));
			Call c = new Call((Proc)lv, vv);
			codes.add(c);
			if (!(((Function)e.left.type).returnType instanceof Void)) {
				v = new Variable();
				((Variable)v).decl = new RecordField(((Function)e.left.type).returnType, local_vars.lastElement());
				codes.add(new Quad(new Op(OpType.ASSIGN), v, RetVal, true));
				c.ret = (Variable) v;
			}
			return v;
		} else if (e.op.optype == OpType.DOT) {
			v = new Variable();
			for (RecordField r: ((Record)e.left.type).fields)
				if (r.symbol.equals(((Var)e.right).sym)) {
					// TODO Optimize
					codes.add(new Quad(new Op(OpType.PLUS), v, lv, false, new Scalar(r.offset), true));
					((Variable)v).decl = new RecordField(new Pointer(r.type), local_vars.lastElement());
					Variable nv = new Variable();
					nv.addr = v;
					nv.decl = new RecordField(r.type, local_vars.lastElement());
					codes.add(new Quad(new Op(OpType.MEMOF), nv, v, true));
					return nv;
				}
		} else if (e.op.optype == OpType.PTR) {
			v = new Variable();
			((Variable)v).decl = new RecordField(((Pointer)e.left.type).elementType, local_vars.lastElement());
			((Variable)v).addr = lv;
			codes.add(new Quad(new Op(OpType.MEMOF), v, lv, true));
			Variable tv = new Variable();
			for (RecordField r: ((Record)((Pointer)e.left.type).elementType).fields)
				if (r.symbol.equals(((Var)e.right).sym)) {
					codes.add(new Quad(new Op(OpType.PLUS), tv, v, false, new Scalar(r.offset), true));
					((Variable)tv).decl = new RecordField(new Pointer(r.type), local_vars.lastElement());
					Variable nv = new Variable();
					nv.addr = tv;
					nv.decl = new RecordField(r.type, local_vars.lastElement());
					codes.add(new Quad(new Op(OpType.MEMOF), nv, tv, true));
					return nv;
				}
		} else {
			v = new Variable();
			rv = trans(e.right);
			((Variable)v).decl = new RecordField(new Int(), local_vars.lastElement());
			if (e.op.optype == OpType.PLUS || e.op.optype == OpType.MINUS) {
				if (lv instanceof Variable && ((Variable)lv).decl.type instanceof Pointer) {
					if (rv instanceof Scalar) {
						Integer k = (Integer) ((Scalar)rv).value * ((Variable)lv).decl.type.size();
						((Scalar)rv).value = k;
					} else if (rv instanceof Variable && !(((Variable)rv).decl.type instanceof Pointer)) {
						codes.add(new Quad(new Op(OpType.TIMES), rv, rv, true, new Scalar(((Variable)lv).decl.type.size()), true));
					}
					((Variable)v).decl.type = ((Variable)lv).decl.type;
				} else if (rv instanceof Variable && ((Variable)rv).decl.type instanceof Pointer) {
					if (lv instanceof Scalar) {
						Integer k = (Integer) ((Scalar)lv).value * ((Variable)rv).decl.type.size();
						((Scalar)lv).value = k;
					} else if (lv instanceof Variable && !(((Variable)lv).decl.type instanceof Pointer)) {
						codes.add(new Quad(new Op(OpType.TIMES), lv, lv, true, new Scalar(((Variable)rv).decl.type.size()), true));
					}
					((Variable)v).decl.type = ((Variable)rv).decl.type;
				}
			}
			codes.add(new Quad(e.op, v, lv, isvalue(lv), rv,isvalue(rv)));
			if (e.op.optype == OpType.MINUS) {
				if (lv instanceof Variable && ((Variable)lv).decl.type instanceof Pointer
				&& rv instanceof Variable && ((Variable)rv).decl.type instanceof Pointer) {
					codes.add(new Quad(new Op(OpType.DIV_ASSIGN), v, v, true, new Scalar(((Variable)rv).decl.type.size()), true));
				}
			}
		}
		return v;
	}
	
	private Term trans(UnaryExpr e) {
		if (e.op.optype == OpType.SIZEOF) {
			return new Scalar(e.expr.type.size());
		}
		Term vv = trans(e.expr);
		Term v = null;
		if (e.op.isSelfOp()) {
			Scalar step = new Scalar(e.expr.type instanceof Pointer ? ((Variable)vv).decl.type.size() : 1);
			if (e.op.toString().startsWith("L")) {
				codes.add(new Quad(new Op(e.op.toString().endsWith("INC") ? OpType.PLUS : OpType.MINUS), vv, vv, true, step, true));
				return vv;
			} else {
				v = new Variable();
				((Variable)v).decl = new RecordField(((Variable)vv).decl.type, local_vars.lastElement());
				codes.add(new Quad(new Op(OpType.ASSIGN), v, vv, true));
				codes.add(new Quad(new Op(e.op.toString().endsWith("INC") ? OpType.PLUS : OpType.MINUS), vv, vv, true, step, true));
			}
		} else if (e.op.optype == OpType.PAREN) {
			Call c = new Call((Proc)vv);
			codes.add(c);
			if (!(((Function)e.expr.type).returnType instanceof Void)) {
				v = new Variable();
				((Variable)v).decl = new RecordField(((Function)e.expr.type).returnType, local_vars.lastElement());
				codes.add(new Quad(new Op(OpType.ASSIGN), v, RetVal, true));
				c.ret = (Variable) v;
			}
		} else if (e.op.optype == OpType.PLUS || e.op.optype == OpType.MINUS) {
			v = new Variable();
			((Variable)v).decl = new RecordField(new Int(), local_vars.lastElement());
			codes.add(new Quad(e.op, v, Zero, true, vv, true));
		} else if (e.op.optype == OpType.TIMES) {
			v = new Variable();
			((Variable)v).decl = new RecordField(((Pointer)e.expr.type).elementType, local_vars.lastElement());
			((Variable)v).addr = vv;
			codes.add(new Quad(new Op(OpType.MEMOF), v, vv, true));
		} else if (e.op.optype == OpType.BIT_AND) {
			v = new Variable();
			((Variable)v).decl = new RecordField(new Int(), local_vars.lastElement());
			codes.add(new Quad(new Op(OpType.ADDR), v, vv, false));
		} else {
			v = new Variable();
			((Variable)v).decl = new RecordField(new Int(), local_vars.lastElement());
			codes.add(new Quad(e.op, v, vv, true));
		}
		return v;
	}
	
	private List<Term> trans(Arg e) {
		List<Term> args = new LinkedList<Term>();
		for (Expr a:e.args)
			args.add(trans(a));
		return args;
	}
	
	private Term trans(Initializer it) {
		if (it instanceof ExprInitializer) {
			return trans(((ExprInitializer) it).expr);
		} else if (it instanceof ArrayInitializer) {
			List<Term> list = new LinkedList<Term>();
			for (Initializer i : ((ArrayInitializer) it).list) {
				list.add(trans(i));
			}
			return new Arr(list);
		}
		return null;
	}
	
	private Term trans(Constant e) {
		if (e instanceof Str) {
			Symbol s = new Symbol("S_" + e.hashCode());
			RecordField r = new RecordField(s, new Array(new Char(), e.val.toString().length() + 1));
			r.init = e.val;
			r.pos = "$gp";
			r.offset = gp_offset.get();
			gp_offset.addAndGet(RecordField.align(r.type.size()));
			global_vars.add(r);
			vscope.put(s, r);
			return trans(s);
		}
		return new Scalar(e.val);
	}
	
	private Term trans(Var e) {
		return trans(e.sym);
	}
	
	private Term trans(Symbol e) {
		Object r = vscope.get(e);
		if (r == null) {
			System.err.println("Unlocated Symbol " + e);
			throw new Error("Unlocated Symbol");
		}
		if (r instanceof RecordField) {
			Variable v = new Variable(e.toString());
			v.decl = (RecordField) r;
			if (((RecordField) r).pos.equals("$gp")) {
				v.addr = "." + v.name;
			}
			if (((RecordField) r).type instanceof Int) {
				Object d = ((RecordField) r).val();
				if (d instanceof Character)
					v.value = (int)((Character)d).charValue();
				else
					v.value = (Integer)d;
			} else if (((RecordField) r).type instanceof Char) {
				Object d = ((RecordField) r).val();
				if (d instanceof Character)
					v.value = (int)((Character)d).charValue();
				else
					v.value = (Integer)d;
			}
			vscope.set(e, v);
			return v;
		} else if (r instanceof Variable) {
			return (Term) r;
		} else if (r instanceof Func) {
			Proc v = (Proc)Label.get(e.toString());
			return v;
		} else if (r instanceof Proc) {
			return (Proc)r;
		}
		System.err.println(e.toString() + "\t" + r);
		return null;
	}
}