package tiger.translate;

import java.util.List;

public class Call extends Operate {
	public Proc addr;
	public Variable ret;
	public List<Term> args;
	public Call(Proc v) {
		addr = v;
	}
	public Call(Proc v, List<Term> a) {
		addr = v;
		args = a;
	}
	public String toString() {
		String tmp = "\tCall " + addr + "(";
		boolean first = false;
		if (args != null) {
			for (Term v : args) {
				tmp += (first ? ", " : "") + v;
				first = true;
			}
		}
		tmp += ")";
		return tmp;
	}
}
