package tiger.translate;

import tiger.ast.*;
import tiger.ast.Op.OpType;

public class Quad extends ICode {
	public Term A, B, C;
	public boolean _B, _C; // is value
	public Op op;
	public Quad(Op o, Term a, Term b, boolean _b, Term c, boolean _c) {
		op = o;
		A = a;
		B = b;
		C = c;
		_B = _b;
		_C = _c;
	}
	public Quad(Op o, Term a, Term b, boolean _b) {
		op = o;
		A = a;
		B = b;
		_B = _b;
	}
	public String toString() {
		String tmp = "\t" + A + " = ";
		if (op == null) {
			tmp += B;
		} else if (C == null) {
			if (op.optype != OpType.ASSIGN)
				tmp += op + " ";
			tmp += B;
		} else {
			tmp += B + " " + op + " " + C;
		}
		return tmp;
	}
}
