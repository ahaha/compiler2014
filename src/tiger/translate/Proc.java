package tiger.translate;

import java.util.List;

import tiger.ast.Func;

public class Proc extends Label {
	public boolean inline;
	public int pid;
	public int stack_size, temp_size, newarg_size;
	public List<Variable> decl;
	public Proc(String name) {
		super(name);
		inline = false;
	}
}
