package tiger.translate;

public class Scalar extends Term {
	public Object value;
	public Scalar(Object v) {
		value = v;
	}
	public String toString() {
		return value.toString();
	}
}
