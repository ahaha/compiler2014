package tiger.translate;

import java.util.List;

public class Arr extends Term {
	List<Term> list;
	public Arr(List<Term> l) {
		list = l;
	}
	public String toString() {
		String tmp = "{";
		boolean first = true;
		for (Term t : list) {
			tmp += (first ? "" : ", ") + t;
			first = false;
		}
		tmp += "}";
		return tmp;
	}
}
