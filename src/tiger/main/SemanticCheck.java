package tiger.main;

import java.io.*;

import com.google.gson.*;

import tiger.semantic.Semantic;
import tiger.syntactic.*;
import tiger.ast.*;

final public class SemanticCheck {

	public static void check(String filename) throws IOException {
		InputStream inp = new FileInputStream(filename);
		parser parser = new parser(inp);
		java_cup.runtime.Symbol parseTree = null;
		try {
			parseTree = parser.parse();
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Error(e.toString());
		} finally {
			inp.close();
		}
		Gson gson = new Gson();
		//String uglys = gson.toJson(parseTree.value);
//		System.out.println(gson.toJson(parseTree.value));
		Semantic checker = null;
		try {
			checker = new Semantic((Program) parseTree.value);
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Error(e.getMessage());
		}
	}
	
	public static void main(String argv[]) throws IOException {
		try {
			check("data.c");
			System.exit(0);
		} catch (Throwable e) {
			System.exit(1);
		}
	}
}
