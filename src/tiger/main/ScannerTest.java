package tiger.main;

import tiger.syntactic.*;
import java_cup.runtime.*;

public final class ScannerTest implements Symbols {

	public static void main(String[] args) throws Exception {
		System.out.println("===ScannerTest : Self-Testing===");
		scan("tests/Data1.c");
		//scan("tests/example2.c");
		//scan("tests/example3.c");
		System.out.println("===ScannerTest : Self-Testing Over===");
	}
	
	public static void scan(String filename) throws Exception {
		System.out.println("==Scanning : " + filename + "==");
		java.io.InputStream in = new java.io.FileInputStream(filename);
		java_cup.runtime.Scanner lexer = new Yylex(in);
		java_cup.runtime.Symbol tok = null;
		
		do {
			tok = lexer.next_token();
			if (tok.sym == NUM) {
				System.out.print(tok.value);
			} else if (tok.sym == CHR) {
				System.out.print("'" + tok.value + "'");
			} else if (tok.sym == ID) {
				System.out.print(tok.value + " ");
			} else if (tok.sym == STR) {
				System.out.print("\"" + tok.value + "\"");
			} else {
				System.out.print(symnames[tok.sym]);
			}
		} while (tok.sym != EOF);
		
		in.close();
	}
	
	static String[] symnames = new String[1000];
	static {
		symnames[TIMES] = "*";
		symnames[DIVIDE] = " / ";
		symnames[LPAREN] = "(";
		symnames[INT] = "int ";
		symnames[MINUS] = " - ";
		symnames[RPAREN] = ") ";
		symnames[SEMICOLON] = ";\n";
		symnames[PLUS] = " + ";
		symnames[ASSIGN] = " = ";
		symnames[IF] = "if ";
		symnames[EOF] = "<EOF>";
		symnames[RETURN] = "return ";
		symnames[error] = "<error>";
		symnames[EQ] = " == ";
		symnames[LBRACE] = "{\n";
		symnames[RBRACE] = "}\n";
		symnames[LT] = " < ";
		symnames[GT] = " > ";
		symnames[COMMA] = ",";
		symnames[LBRACKET] = "[";
		symnames[RBRACKET] = "]";
		symnames[STRUCT] = "struct ";
		symnames[CHAR] = "char";
		symnames[BIT_AND] = "&";
		symnames[BIT_OR] = "|";
		symnames[FOR] = "for";
		symnames[WHILE] = "while";
		symnames[LE] = "<=";
		symnames[GE] = ">=";
		symnames[DOT] = ".";
		symnames[ELSE] = "else";
		symnames[BREAK] = "break";
		symnames[ADD_ASSIGN] = "+=";
		symnames[SUB_ASSIGN] = "-=";
		symnames[MUL_ASSIGN] = "*=";
		symnames[DIV_ASSIGN] = "/=";
		symnames[MOD_ASSIGN] = "%=";
		symnames[INC]	= "++";
		symnames[DEC]	= "--";
		symnames[MODULO] = "%";
	}
}