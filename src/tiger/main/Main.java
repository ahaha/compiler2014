package tiger.main;

import java.io.*;

import com.google.gson.*;

import tiger.mips.*;
import tiger.semantic.Semantic;
import tiger.syntactic.*;
import tiger.translate.Translate;
import tiger.ast.*;

final public class Main {
	
	private static int DEBUG = 0;

	public static void compile(String input, String output) throws Exception {
		InputStream inp = new FileInputStream(input);
		OutputStream oup = new FileOutputStream(output);
		parser parser = new parser(inp);
		java_cup.runtime.Symbol parseTree = null;
		try {
			parseTree = parser.parse();
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Error(e.toString());
		} finally {
			inp.close();
		}
		Gson gson = new Gson();
		gson.toJson(parseTree.value);
		if (DEBUG >= 3)
			System.err.println(gson.toJson(parseTree.value));
		new Semantic((Program) parseTree.value);
		Translate translator = new Translate((Program)parseTree.value);
		PrintWriter out = new PrintWriter(oup);
		if (DEBUG >= 0) {
			SuperMips spim = new SuperMips(translator.codes, translator.global_vars, DEBUG);
			out.println(spim);
			System.out.println(spim);
		}
		if (DEBUG < 0) {
			EasyMips mips = new EasyMips(translator.codes, translator.global_vars, DEBUG);
			out.println(mips);
			System.out.println(mips);
		}
		out.close();
	}
	
	public static void main(String argv[]) throws IOException {
		try {
			compile("data.c", "data.s");
			System.exit(0);
		} catch (Throwable e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
}
