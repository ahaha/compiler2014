package tiger.ast;

import java.util.List;

import tiger.type.RecordField;

public class Decl extends Def {
	public TypeSpecifier typespecifier;
	public Declarators declarators;
	public List<RecordField> list;

	public Decl(TypeSpecifier t, InitDeclarator d) {
		declarators = new Declarators(d);
		typespecifier = t;
	}
	
	public Decl(TypeSpecifier t, Declarators d) {
		declarators = d;
		typespecifier = t;
	}
	
	public Decl(TypeSpecifier t) {
		declarators = null;
		typespecifier = t;
	}
	
}
