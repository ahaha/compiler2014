package tiger.ast;

public class WhileStmt extends IterationStmt {

	public Expr testExpr;
	public Stmt stmt;
	
	public WhileStmt(Expr t, Stmt s) {
		testExpr = t;
		stmt = s;
	}
}
