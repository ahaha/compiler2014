package tiger.ast;

import tiger.type.Void;

public class VoidType extends TypeSpecifier {
	public VoidType() {
		type = new Void();
	}
}
