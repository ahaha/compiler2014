package tiger.ast;

public class ReturnStmt extends JumpStmt {
	public Expr retExpr;
	
	public ReturnStmt() {
		retExpr = null;
	}
	
	public ReturnStmt(Expr r) {
		retExpr = r;
	}
}
