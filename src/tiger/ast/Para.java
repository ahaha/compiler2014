package tiger.ast;

import java.util.LinkedList;
import java.util.List;

public class Para {
	public List<PlainDecl> list;
	
	public Para(PlainDecl d) {
		list = new LinkedList<PlainDecl>();
		list.add(d);
	}
	
	public Para(Para p, PlainDecl d) {
		list = p.list;
		list.add(d);
	}
}
