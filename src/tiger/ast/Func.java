package tiger.ast;

import java.util.LinkedList;
import java.util.List;

import tiger.translate.Variable;
import tiger.type.*;

public class Func extends Def {
	public TypeSpecifier typespecifier;
	public PlainDeclarator declarator;
	public Para	para;
	public CompoundStmt stmt;
	public List<RecordField> args;
	public Type type;
	
	public Func(TypeSpecifier t, PlainDeclarator d, Para p, CompoundStmt s) {
		typespecifier = t;
		declarator = d;
		para = p;
		stmt = s;
		args = new LinkedList<RecordField>();
	}
}
