package tiger.ast;

import java.util.LinkedList;
import java.util.List;

public class Program {

	public List<Def> list;

	public Program(Def d) {
		list = new LinkedList<Def>();
		list.add(d);
	}
	
	public Program(Program p, Def d) {
		list = p.list;
		list.add(d);
	}
	
}
