package tiger.ast;

import tiger.type.Int;

public class IntType extends TypeSpecifier {
	public IntType() {
		type = new Int();
	}
}
