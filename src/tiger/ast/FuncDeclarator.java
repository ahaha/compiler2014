package tiger.ast;

public class FuncDeclarator extends Declarator {
	
	public Para para;
	
	public FuncDeclarator(PlainDeclarator pd, Para p) {
		super(pd);
		para = p;
	}
	
	public FuncDeclarator(PlainDeclarator pd) {
		super(pd);
		para = null;
	}
}
