package tiger.ast;

import tiger.type.Char;

public class Chr extends Constant {

	public Chr(char charValue) {
		val = new Character(charValue);
		isLvalue = false;
		type = new Char();
	}
}
