package tiger.ast;

public class InitDeclarator {
	public Var id;
	public Declarator declarator;
	public Initializer initializer;
	
	public InitDeclarator(Declarator d) {
		if (d != null)
			id = d.id;
		initializer = null;
		declarator = d;
	}
	
	public InitDeclarator(Declarator d, Initializer i) {
		if (d != null)
			id = d.id;
		initializer = i;
		declarator = d;
	}
}
