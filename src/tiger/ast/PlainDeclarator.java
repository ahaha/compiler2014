package tiger.ast;

public class PlainDeclarator extends Declarator {
	
	public PlainDeclarator(Var i) {
		super(null);
		id = i;
	}
	
	public PlainDeclarator(PlainDeclarator p) {
		super(p);
	}
}
