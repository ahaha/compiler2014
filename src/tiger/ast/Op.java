package tiger.ast;

import java.lang.reflect.Method;
import java.util.HashMap;

public class Op extends Expr{
	public OpType optype;
	static public Calc calc = new Calc();
	
	public Op(OpType o) {
		optype = o;
	}
	
	public String toString() {
		return optype.toString();
	}
	
	static class Calc {
		public int PLUS(int a, int b) {
			return a + b;
		}
		public int MINUS(int a, int b) {
			return a - b;
		}
		public int TIMES(int a, int b) {
			return a * b;
		}
		public int DIVIDE(int a, int b) {
			return a / b;
		}
		public int MODULO(int a, int b) {
			return a % b;
		}
		public int SHL(int a, int b) {
			return a << b;
		}
		public int SHR(int a, int b) {
			return a >> b;
		}
		public int BIT_XOR(int a, int b) {
			return a ^ b;
		}
		public int BIT_OR(int a, int b) {
			return a | b;
		}
		public int BIT_AND(int a, int b) {
			return a & b;
		}
		public int OR(int a, int b) {
			return a != 0 || b != 0 ? 1 : 0;
		}
		public int AND(int a, int b) {
			return a != 0 && b != 0 ? 1 : 0;
		}
		public int GT(int a, int b) {
			return a > b ? 1 : 0;
		}
		public int LT(int a, int b) {
			return a < b ? 1 : 0;
		}
		public int GE(int a, int b) {
			return a >= b ? 1 : 0;
		}
		public int LE(int a, int b) {
			return a <= b ? 1 : 0;
		}
		public int EQ(int a, int b) {
			return a == b ? 1 : 0;
		}
		public int NE(int a, int b) {
			return a != b ? 1 : 0;
		}
	}
		
	public static enum OpType {
		ADD_ASSIGN, AND, AND_ASSIGN, ASSIGN, MEMOF, BIT_AND, BIT_NOT, BIT_OR, BIT_XOR,
		BRACKET, CAST, COMMA, DIV_ASSIGN, DIVIDE, DOT, EQ, GE, GT, LDEC, LE, LINC,
		LT, MINUS, MOD_ASSIGN, MODULO, MUL_ASSIGN, NE, NOT, OR, OR_ASSIGN, PAREN,
		PLUS, PTR, RDEC, RINC, SHL, SHL_ASSIGN, SHR, SHR_ASSIGN, SIZEOF, SUB_ASSIGN,
		TIMES, XOR_ASSIGN, GTZ, LEZ, ADDR;
	}
	
	public int Operate(Object v1, Object v2) {
		try {
			int a = (Integer) (v1 instanceof Character ? (int)((Character)v1).charValue() : v1);
			int b = (Integer) (v2 instanceof Character ? (int)((Character)v2).charValue() : v2);
			Method c = calc.getClass().getMethod(optype.toString(), int.class, int.class);
			return (Integer)c.invoke(calc, a, b);
		} catch (Throwable e) {
			e.printStackTrace();
			System.err.println("Undefined Function " + optype);
		}
		return 0;
	}
	
	public String asmrr() {
		if (optype == OpType.PLUS)
			return "add";
		else if (optype == OpType.AND)
			return "and";
		else if (optype == OpType.DIVIDE)
			return "div";
		else if (optype == OpType.TIMES)
			return "mul";
		else if (optype == OpType.NOT)
			return "not";
		else if (optype == OpType.OR)
			return "or";
		else if (optype == OpType.MODULO)
			return "rem";
		else if (optype == OpType.SHL)
			return "sllv";
		else if (optype == OpType.SHR)
			return "srlv";
		else if (optype == OpType.MINUS)
			return "sub";
		else if (optype == OpType.BIT_XOR)
			return "xor";
		else if (optype == OpType.BIT_OR)
			return "or";
		else if (optype == OpType.BIT_AND)
			return "and";
		else if (optype == OpType.GE)
			return "sge";
		else if (optype == OpType.LE)
			return "sle";
		else if (optype == OpType.GT)
			return "sgt";
		else if (optype == OpType.LT)
			return "slt";
		else if (optype == OpType.EQ)
			return "seq";
		else if (optype == OpType.NE)
			return "sne";
//		System.err.println(optype);
		return "";
	}
	
	public String asmri() {
		if (optype == OpType.PLUS)
			return "addiu";
		else if (optype == OpType.AND)
			return "andi";
		else if (optype == OpType.OR)
			return "ori";
		else if (optype == OpType.SHL)
			return "sll";
		else if (optype == OpType.SHR)
			return "srl";
		else if (optype == OpType.BIT_XOR)
			return "xori";
		else if (optype == OpType.BIT_OR)
			return "ori";
		else if (optype == OpType.BIT_AND)
			return "andi";
		else if (optype == OpType.LT)
			return "slti";
//		System.err.println(optype);
		return "";
	}
	
	public String asmr(Object... args) {
		if (optype == OpType.ASSIGN)
			return "move";
		else if (optype == OpType.ADDR)
			return "la";
		else if (optype == OpType.MEMOF)
			return "lw";
		else if (optype == OpType.BIT_NOT)
			return "not";
//		System.err.println(optype);
		return "";
	}
	
	public boolean isAssign() {
		return	optype == OpType.ASSIGN
			||	optype == OpType.ADD_ASSIGN
			||	optype == OpType.AND_ASSIGN
			||	optype == OpType.DIV_ASSIGN
			||	optype == OpType.MOD_ASSIGN
			||	optype == OpType.DIV_ASSIGN
			||	optype == OpType.MUL_ASSIGN
			||	optype == OpType.OR_ASSIGN
			||	optype == OpType.SHL_ASSIGN
			||	optype == OpType.SHR_ASSIGN
			||	optype == OpType.SUB_ASSIGN
			||	optype == OpType.XOR_ASSIGN
			;
	}
	
	public boolean isCompare() {
		return	optype == OpType.EQ
			||	optype == OpType.NE
			||	optype == OpType.GE
			||	optype == OpType.LE
			||	optype == OpType.GT
			||	optype == OpType.LT
			;
	}
	
	public boolean isLogical() {
		return	optype == OpType.AND
			||	optype == OpType.OR
			;
	}
	
	public boolean isArithmetic() {
		return	optype == OpType.PLUS
			||	optype == OpType.MINUS
			||	optype == OpType.TIMES
			||	optype == OpType.DIVIDE
			||	optype == OpType.MODULO
			||	optype == OpType.SHL
			||	optype == OpType.SHR
			||	optype == OpType.BIT_AND
			||	optype == OpType.BIT_OR
			||	optype == OpType.BIT_XOR
			;
	}
	
	public boolean isSelfOp() {
		return	optype == OpType.LINC
			||	optype == OpType.RINC
			||	optype == OpType.LDEC
			||	optype == OpType.RDEC
			;
	}
	
	public Op reduce() {
		if (optype == OpType.ADD_ASSIGN) {
			return new Op(OpType.PLUS);
		} else if (optype == OpType.SUB_ASSIGN) {
			return new Op(OpType.MINUS);
		} else if (optype == OpType.MUL_ASSIGN) {
			return new Op(OpType.TIMES);
		} else if (optype == OpType.DIV_ASSIGN) {
			return new Op(OpType.DIVIDE);
		} else if (optype == OpType.MOD_ASSIGN) {
			return new Op(OpType.MODULO);
		} else if (optype == OpType.AND_ASSIGN) {
			return new Op(OpType.AND);
		} else if (optype == OpType.OR_ASSIGN) {
			return new Op(OpType.OR);
		} else if (optype == OpType.XOR_ASSIGN) {
			return new Op(OpType.BIT_XOR);
		} else if (optype == OpType.SHL_ASSIGN) {
			return new Op(OpType.SHL);
		} else if (optype == OpType.SHR_ASSIGN) {
			return new Op(OpType.SHR);
		}
		return this;
	}
	
	public Op not() {
		if (optype == OpType.EQ)
			return new Op(OpType.NE);
		if (optype == OpType.NE)
			return new Op(OpType.EQ);
		if (optype == OpType.GT)
			return new Op(OpType.LE);
		if (optype == OpType.LE)
			return new Op(OpType.GT);
		if (optype == OpType.GE)
			return new Op(OpType.LT);
		if (optype == OpType.LT)
			return new Op(OpType.GE);
		if (optype == OpType.GTZ)
			return new Op(OpType.LEZ);
		if (optype == OpType.LEZ)
			return new Op(OpType.GTZ);
		return this;
	}
}
