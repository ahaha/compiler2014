package tiger.ast;

import tiger.type.Type;

public abstract class Expr extends Stmt {
	public boolean isLvalue;
	public Object val;
	public boolean isConstant() {
		return val != null;
	}
	public Type type;
}
