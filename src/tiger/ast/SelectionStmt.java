package tiger.ast;

public class SelectionStmt extends Stmt {
	
	public Expr ifExpr;
	public Stmt thenStmt, elseStmt;

	public SelectionStmt(Expr i, Stmt t) {
		ifExpr = i;
		thenStmt = t;
		elseStmt = null;
	}
	
	public SelectionStmt(Expr i, Stmt t, Stmt e) {
		ifExpr = i;
		thenStmt = t;
		elseStmt = e;
	}
}
