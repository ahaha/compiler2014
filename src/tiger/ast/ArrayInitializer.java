package tiger.ast;

import java.util.LinkedList;
import java.util.List;

public class ArrayInitializer extends Initializer {
	public List<Initializer> list;
	
	public ArrayInitializer(Initializer i) {
		list = new LinkedList<Initializer>();
		list.add(i);
	}
	
	public ArrayInitializer(ArrayInitializer a, Initializer i) {
		list = a.list;
		list.add(i);
	}
}
