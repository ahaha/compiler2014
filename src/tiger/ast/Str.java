package tiger.ast;

import tiger.type.Array;
import tiger.type.Char;

public class Str extends Constant {
	
	public Str(String strValue) {
		val = strValue;
		isLvalue = false;
		type = new Array(new Char(), strValue.length() + 1); // null byte
	}

}
