package tiger.ast;

public class CompoundStmt extends Stmt {
	public Decls decls;
	public Stmts stmts;
	
	public CompoundStmt(Decls d, Stmts s) {
		decls = d;
		stmts = s;
	}
}
