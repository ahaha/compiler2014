package tiger.ast;

import tiger.symbol.Symbol;

public class StructType extends TypeSpecifier {
	public Var id;
	public Decls decls;
	
	public StructType(Var i) {
		id = i;
		decls = null;
	}
	
	public StructType(Decls p) {
		id = new Var(new Symbol(null));
		decls = p;
	}
	
	public StructType(Var i, Decls p) {
		id = i;
		decls = p;
	}
}
