package tiger.ast;

public class ArrayDeclarator extends Declarator {

	public ConstArray constarray;
	
	public ArrayDeclarator(PlainDeclarator pd, ConstArray a) {
		super(pd);
		constarray = a;
	}
	
}
