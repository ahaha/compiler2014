package tiger.ast;

public class BinaryExpr extends Expr {
	public Op op;
	public Expr left, right;

	public BinaryExpr(Op o, Expr l, Expr r) {
		op = o;
		left = l;
		right = r;
	}
}