package tiger.ast;

import tiger.type.Int;

public class Num extends Constant {

	public Num(int intValue) {
		val = new Integer(intValue);
		isLvalue = false;
		type = new Int();
	}
}
