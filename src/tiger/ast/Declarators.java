package tiger.ast;

import java.util.LinkedList;
import java.util.List;

public class Declarators {
	public List<InitDeclarator> list;
	
	public Declarators(InitDeclarator d) {
		list = new LinkedList<InitDeclarator>();
		list.add(d);
	}
	
	public Declarators(Declarators ds, InitDeclarator d) {
		list = ds.list;
		list.add(d);
	}
}
