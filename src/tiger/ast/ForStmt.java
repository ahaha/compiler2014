package tiger.ast;

public class ForStmt extends IterationStmt {

	public Expr initExpr, testExpr, stepExpr;
	public Stmt stmt;
	
	public ForStmt(Expr i, Expr t, Expr s, Stmt st) {
		initExpr = i;
		testExpr = t;
		stepExpr = s;
		stmt = st;
	}
}
