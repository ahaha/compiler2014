package tiger.ast;

import java.util.LinkedList;
import java.util.List;

public class ConstArray {
	public List<Expr> list;
	
	public ConstArray(Expr e) {
		list = new LinkedList<Expr>();
		list.add(e);
	}
	
	public ConstArray(ConstArray c, Expr e) {
		list = c.list;
		list.add(e);
	}
}
