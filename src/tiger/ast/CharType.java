package tiger.ast;

import tiger.type.Char;

public class CharType extends TypeSpecifier {
	public CharType() {
		type = new Char();
	}
}
