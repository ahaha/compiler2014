package tiger.ast;

public class MultiDecl extends Decl {

	public MultiDecl(TypeSpecifier t, Declarators d) {
		super(t, d);
	}

	public MultiDecl(TypeSpecifier t, InitDeclarator d) {
		super(t, d);
	}
}
