package tiger.ast;

public class PlainDecl extends Decl {

	public PlainDecl(TypeSpecifier t, Declarators d) {
		super(t, d);
	}

	public PlainDecl(TypeSpecifier t, Declarator d) {
		super(t, d);
	}
}
