package tiger.ast;

public class UnaryExpr extends Expr {
	public Op op;
	public Expr expr;
	
	public UnaryExpr(Op o, Expr e) {
		op = o;
		expr = e;
	}
}
