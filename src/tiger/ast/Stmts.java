package tiger.ast;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Stmts {
	
	public List<Stmt> list;
	
	public Stmts() {
		list = new ArrayList<Stmt>();
	}
	
	public Stmts(Stmts ss, Stmt s) {
		list = ss.list;
		list.add(s);
	}
}
