package tiger.ast;

import tiger.symbol.Symbol;

public class UnionType extends TypeSpecifier {
	public Var id;
	public Decls decls;
	
	public UnionType(Var i) {
		id = i;
		decls = null;
	}
	
	public UnionType(Decls p) {
		id = new Var(new Symbol(null));
		decls = p;
	}
	
	public UnionType(Var i, Decls p) {
		id = i;
		decls = p;
	}
}
