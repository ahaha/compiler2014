package tiger.ast;

import java.util.LinkedList;
import java.util.List;

public class Decls {
	
	public List<Decl> list;
	
	public Decls() {
		list = new LinkedList<Decl>();
	}
	
	public Decls(Decl d) {
		list = new LinkedList<Decl>();
		list.add(d);
	}
	
	public Decls(Decls ds, Decl d) {
		list = ds.list;
		list.add(d);
	}
}
