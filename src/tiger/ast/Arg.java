package tiger.ast;

import java.util.LinkedList;
import java.util.List;

public class Arg extends Expr {
	public int size;
	public List<Expr> args;
	
	public Arg(Expr e) {
		args = new LinkedList<Expr>();
		args.add(e);
	}
	
	public Arg(Arg a, Expr e) {
		args = a.args;
		args.add(e);
	}
}
