package tiger.semantic;

import tiger.type.*;

/**
 * In a real project, you may need to add typing information here.
 */
public class VarInfo {

	public Type type;
	public Temp loc;
	
	public VarInfo(Type t) {
		type = t;
	}

	public VarInfo(Type t, int offset) {
		loc = new Temp(offset);
		type = t;
	}
}
