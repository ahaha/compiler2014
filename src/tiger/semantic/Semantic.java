package tiger.semantic;

import static tiger.symbol.Symbol.symbol;

import java.util.LinkedList;
import java.util.List;

import tiger.ast.*;
import tiger.ast.Op.OpType;
import tiger.symbol.Symbol;
import tiger.symbol.Table;
import tiger.type.*;
import tiger.type.Void;


public class Semantic {
	private Table vscope;
	private Table tscope;
	private int loop;
	private Type functype;
	
	public Semantic(Program program) {
		vscope = new Table();
		tscope = new Table();
		loop = 0;
		vscope.put(symbol("printf"), new VarInfo(new Function(new Void(), true)));
		vscope.put(symbol("scanf"), new VarInfo(new Function(new Int(), true)));
		List<Type> list = new LinkedList<Type>();
		list.add(new Int());
		vscope.put(symbol("malloc"), new VarInfo(new Function(new Pointer(new Void()), list)));
		check(program);
	}
	
	public void check(Program prog) {
		for(Def p: prog.list) {
			if (p instanceof Decl)
				check((Decl)p);
			else if (p instanceof Func)
				check((Func)p);
		}
	}
	
	public List<RecordField> check(Decl decl) {
		check(decl.typespecifier);
		Type type = decl.typespecifier.type;
		//System.out.print(type);
		LinkedList<RecordField> list = new LinkedList<RecordField>();
		// Only definition
		if (decl.declarators == null) {
			decl.list = list;
			return list;
		}
		for (InitDeclarator dd : decl.declarators.list) {
			Symbol sym = dd.id.sym;
			// Declaration conflict
			if (vscope.isConflict(sym)) {
				throw new Error("Declaration conflict");
			}
			Declarator d = (Declarator) (dd instanceof Declarator ? dd : dd.declarator);
			PlainDeclarator pd = (PlainDeclarator) d.declarator;
			Type t = type;
			if (pd.declarator != null)
				t = new Pointer(type, (PlainDeclarator) pd.declarator);
			// Undefined Struct or Union Type
			if (t instanceof Name) {
				throw new Error("Undefined Type");
			}
			// Void Type
			if (t instanceof Void) {
				throw new Error("Can't define void type variable");
			}
			//System.out.println(d.getClass().toString() + " " + sym);
			if (d instanceof ArrayDeclarator) {
				// TODO dimension
				for (Expr e: ((ArrayDeclarator) d).constarray.list) {
					check(e);
					if (!e.isConstant())
						throw new Error("Array constructor must be constant integer");
					if (t instanceof Array) {
						t = new Array(new Array(((Array) t).elementType, (Integer)e.val), ((Array) t).capacity);
					} else {
						t = new Array(t, (Integer)e.val);
					}
				}
			} else if (d instanceof FuncDeclarator) {
				Function f = new Function(t);
				vscope.beginScope();
				if (((FuncDeclarator) d).para != null) {
					List<RecordField> args = check(((FuncDeclarator) d).para);
					for (RecordField r:args)
						f.argumentType.add(r.type);
				}
				vscope.endScope();
				t = f;
			}
			
			vscope.put(sym, new VarInfo(t));
			
			// check initializer
			if (dd instanceof InitDeclarator) {
				Initializer it = dd.initializer;
				if (it != null) {
					check(it);
					if (!Type.cast(it.type, t))
						throw new Error("Initializer Type Mismatch");
				}
			}
			RecordField r = new RecordField(sym, t);
			r.init = dd.initializer;
			list.add(r);
		}
		decl.list = list;
		return list;
	}
	
	public void check(TypeSpecifier t) {
		t.isLvalue = false;
		if (t instanceof UnionType) {
			Union u = (Union) tscope.get(((UnionType) t).id.sym);
			if (u == null) {
				if (((UnionType) t).decls == null) {
					t.type = new Name(((UnionType) t).id.sym.toString());
					return ;
				} else {
					u = new Union();
					tscope.put(((UnionType) t).id.sym, u);
				}
			} else {
				//Declaration conflict
				if (((UnionType) t).decls != null && tscope.isConflict(((UnionType) t).id.sym))
					throw new Error("Declaration conflict");
			}
			vscope.beginScope();
			if (((UnionType) t).decls != null) {
				for (Decl d : ((UnionType) t).decls.list) {
					u.fields.addAll(check(d));
				}
			}
			vscope.endScope();
			int size = 0;
			for (RecordField r : u.fields) {
				size = Math.max(r.type.size(), size);
			}
			u.sz = size;
			u.symbol = ((UnionType) t).id.sym;
			t.type = u;
		} else if (t instanceof StructType) {
			Struct s = (Struct) tscope.get(((StructType) t).id.sym);
			if (s == null) {
				if (((StructType) t).decls == null) {
					t.type = new Name(((StructType) t).id.sym.toString());
					return ;
				} else {
					s = new Struct();
					tscope.put(((StructType) t).id.sym, s);
				}
			} else {
				//Declaration conflict
				if (((StructType) t).decls != null && tscope.isConflict(((StructType) t).id.sym))
					throw new Error("Declaration conflict");
			}
			vscope.beginScope();
			if (((StructType) t).decls != null) {
				for (Decl d : ((StructType) t).decls.list) {
					s.fields.addAll(check(d));
				}
			}
			vscope.endScope();
			// offset for Struct
			int offset = 0;
			for (RecordField r : s.fields) {
				r.offset = offset;
				offset += r.type.size();
				offset = RecordField.align(offset);
			}
			s.sz = offset;
			s.symbol = ((StructType) t).id.sym;
			t.type = s;
		} else if (t instanceof PointerType) {
			check(((PointerType) t).type_specifier);
			t.type = new Pointer(((PointerType)t).type_specifier.type);
		}
	}
	
	public void check(Func func) {
		//Declaration conflict
		if (vscope.isConflict(func.declarator.id.sym))
			throw new Error("Declaration conflict");
		check(func.typespecifier);
		functype = func.typespecifier.type;
		if (func.declarator.declarator != null)
			functype = new Pointer(functype, (PlainDeclarator) func.declarator.declarator);
		if (functype instanceof Name)
			throw new Error("Return Type Must Be Defined");
		Function type = new Function(functype);
		func.type = type;
		vscope.put(func.declarator.id.sym, new VarInfo(type)); // correct it later
		vscope.beginScope();
		tscope.beginScope();
		if (func.para != null) {
			List<RecordField> args = check(func.para);
			for (RecordField r: args) {
				type.argumentType.add(r.type);
			}
			func.args = args;
		}
		if (func.stmt.decls != null) {
			for (Decl d: func.stmt.decls.list) {
				check(d);
			}
		}
		if (func.stmt.stmts != null) {
			for (Stmt s: func.stmt.stmts.list) {
				check(s);
			}
		}		
		vscope.endScope();
		tscope.endScope();
		// check return
	}
	
	public List<RecordField> check(Para para) {
		LinkedList<RecordField> args = new LinkedList<RecordField>();
		for (PlainDecl p: para.list) {
			args.addAll(check(p));
		}
		return args;
	}
	
	public void check(Stmt stmt) {
		if (stmt instanceof Expr) {
			check((Expr)stmt);
		} else if (stmt instanceof CompoundStmt) {
			check((CompoundStmt)stmt);
		} else if (stmt instanceof SelectionStmt) {
			check((SelectionStmt)stmt);
		} else if (stmt instanceof IterationStmt) {
			check((IterationStmt)stmt);
		} else if (stmt instanceof JumpStmt) {
			check((JumpStmt)stmt);
		}
	}
	
	public void check(Expr e) {
		if (e instanceof BinaryExpr) {
			check((BinaryExpr)e);
		} else if (e instanceof UnaryExpr) {
			check((UnaryExpr)e);
		} else if (e instanceof Constant) {
			check((Constant)e);
		} else if (e instanceof TypeSpecifier) {
			check((TypeSpecifier)e);
		} else if (e instanceof Arg) {
			check((Arg)e);
		} else if (e instanceof Initializer) {
			check((Initializer)e);
		} else if (e instanceof Var) {
			check((Var)e);
		}
	}
	
	public void check(CompoundStmt stmt) {
		vscope.beginScope();
		tscope.beginScope();
		if (stmt.decls != null) {
			for (Decl d: stmt.decls.list) {
				check(d);
			}
		}
		if (stmt.stmts != null) {
			for (Stmt s: stmt.stmts.list) {
				check(s);
			}
		}
		vscope.endScope();
		tscope.endScope();
	}
	
	public void check(SelectionStmt stmt) {
		check(stmt.ifExpr);
		if (!Type.cast(stmt.ifExpr.type, new Int()))
			throw new Error("Can't determine the value of " + stmt.ifExpr.type);
		check(stmt.thenStmt);
		if (stmt.elseStmt != null)
			check(stmt.elseStmt);
	}
	
	public void check(IterationStmt stmt) {
		if (stmt instanceof ForStmt) {
			if (((ForStmt) stmt).initExpr != null)
				check(((ForStmt) stmt).initExpr);
			if (((ForStmt) stmt).testExpr != null)
				check(((ForStmt) stmt).testExpr);
			if (!Type.cast(((ForStmt) stmt).testExpr.type, new Int()))
				throw new Error("Can't determine the value of " + ((ForStmt) stmt).testExpr.type);
			if (((ForStmt) stmt).stepExpr != null)
				check(((ForStmt) stmt).stepExpr);
			loop++;
			if (((ForStmt) stmt).stmt != null)
				check(((ForStmt) stmt).stmt);
			loop--;
		} else if (stmt instanceof WhileStmt) {
			check(((WhileStmt) stmt).testExpr);
			if (!Type.cast(((WhileStmt) stmt).testExpr.type, new Int()))
				throw new Error("Can't determine the value of " + ((WhileStmt) stmt).testExpr.type);
			loop++;
			check(((WhileStmt) stmt).stmt);
			loop--;
		}
	}
	
	public void check(JumpStmt stmt) {
		if (stmt instanceof BreakStmt) {
			// Error Break
			if (loop == 0)
				throw new Error("Error Break");
		} else if (stmt instanceof ContinueStmt) {
			// Error Continue
			if (loop == 0)
				throw new Error("Error Continue");
		} else if (stmt instanceof ReturnStmt) {
			Type type = null;
			if (((ReturnStmt) stmt).retExpr != null) {
				check(((ReturnStmt) stmt).retExpr);
				type = ((ReturnStmt) stmt).retExpr.type;
			} else {
				type = new Void();
			}
			//check return type
			if (!Type.cast(type, functype))
				throw new Error("Return Type Mismatch");
		}
	}
	
	public void check(BinaryExpr e) {
		check(e.left);
		if (e.op.optype != Op.OpType.DOT && e.op.optype != Op.OpType.PTR) {
			check(e.right);
		}
		e.isLvalue = false;
		Type ltype = e.left.type, rtype = e.right.type;
		boolean matched = false;
		if (e.op.isAssign()) {		//check assignment expression
			//check Lvalue
			if (!e.left.isLvalue)
				throw new Error("Invalid Lvalue");
			//check type_specifier match
			if (Type.cast(ltype, rtype)) {
				matched = true;
			} else if (ltype instanceof Pointer) {
				System.err.println(((Var)(((BinaryExpr)e.right).left)).sym);
				System.err.println(rtype.getClass());
				if ((rtype instanceof Pointer || rtype instanceof Basic) && e.op.optype == Op.OpType.ASSIGN) {
					matched = true; // assignment in same type_specifier
				} else if (rtype instanceof Basic && (e.op.optype == Op.OpType.ADD_ASSIGN || e.op.optype == Op.OpType.SUB_ASSIGN)) {
					matched = true; // += / -=
				}
			} else if (ltype.equals(rtype) && e.op.optype == Op.OpType.ASSIGN) {
				matched = true;
			}
			e.isLvalue = true;
			e.type = ltype;
		} else if (e.op.isCompare()){
			//Can't compare between Structs or Unions
			if (ltype instanceof Record || rtype instanceof Record)
				throw new Error("Can't compare between Structs or Unions");
			// TODO compare between more types
			matched = true;
			e.type = new Int();
			if (e.left.isConstant() && e.right.isConstant()) {
				e.val = e.op.Operate(e.left.val, e.right.val);
			}
		} else if (e.op.isLogical()) {
			// TODO check more types
			matched = true;
			e.type = new Int();
		} else if (e.op.isArithmetic()) {
			if (e.op.optype == OpType.PLUS) {
				if (ltype instanceof Basic || rtype instanceof Basic) {
						matched = true;
						e.type = ltype;
						if (e.left.isConstant() && e.right.isConstant()) {
							e.val = e.op.Operate(e.left.val, e.right.val);
						}
				}
				if (ltype instanceof Pointer && rtype instanceof Basic) {
					matched = true;
					e.type = new Pointer(((Pointer)ltype).elementType);
				}
			} else if (e.op.optype == OpType.MINUS) {
				if (ltype instanceof Basic || rtype instanceof Basic) {
					matched = true;
					e.type = ltype;
					if (e.left.isConstant() && e.right.isConstant()) {
						e.val = e.op.Operate(e.left.val, e.right.val);
					}
				}
				if (ltype instanceof Array && rtype.equals(ltype)) {
					matched = true;
					e.type = new Int();
				}
				if (ltype instanceof Pointer && rtype.equals(ltype)) {
					matched = true;
					e.type = new Int();
				}
				if (ltype instanceof Function && rtype.equals(ltype)) {
					matched = true;
					e.type = new Int();
				}
 			} else {
				// arithmetic expression requires interger expression 
				if (ltype instanceof Basic && rtype instanceof Basic) {
					matched = true;
					e.type = new Int();
					if (e.left.isConstant() && e.right.isConstant()) {
						e.val = e.op.Operate(e.left.val, e.right.val);
					}
				}
			}
		} else if (e.op.optype == OpType.COMMA) {
			e.type = rtype;
			e.val = e.right.val;
			matched = true;
		} else if (e.op.optype == OpType.CAST) {
			e.type = ltype;
			e.isLvalue = e.right.isLvalue;
			if (ltype instanceof Void) {
				matched = true;
			}
			if (Type.cast(rtype, ltype)) {
				matched = true;
			}
			if (ltype.equals(rtype) && !(ltype instanceof Record)) {
				matched = true;
			}
			// TODO more casting rules
		} else if (e.op.optype == OpType.BRACKET) {
			e.isLvalue = true;
			if (rtype instanceof Basic) {
				if (ltype instanceof Pointer) {
					matched = true;
					e.type = ((Pointer) ltype).elementType;
				}
			}
		} else if (e.op.optype == OpType.PAREN) {
			if (ltype instanceof Function && e.right instanceof Arg) {
				LinkedList<Type> list = new LinkedList<Type>();
				for (Expr arg: ((Arg)e.right).args)
					list.add(arg.type);
				matched = ltype.equals(new Function(((Function) ltype).returnType, list));
				e.type = ((Function)ltype).returnType;
				e.isLvalue = false; // TODO check isLvalue
			}
		} else if (e.op.optype == OpType.DOT) {
			if (ltype instanceof Record && e.right instanceof Var) {
				for (RecordField r: ((Record)ltype).fields)
					if (r.symbol.equals(((Var)e.right).sym)) {
						matched = true;
						e.type = r.type;
						e.isLvalue = true; // TODO check isLvalue
						break;
					}
			}
		} else if (e.op.optype == OpType.PTR) {
			if (ltype instanceof Pointer && e.right instanceof Var) {
				if (((Pointer)ltype).elementType instanceof Record) {
					for (RecordField r : ((Record)((Pointer)ltype).elementType).fields) {
						if (r.symbol.equals(((Var)e.right).sym)) {
							matched = true;
							e.type = r.type;
							e.isLvalue = true; // TODO check isLvalue
							break;
						}
					}
				}
			}
		}
		if (!matched)
			throw new Error("Type Mismatched " + e.left + " " + e.op);
	}
	
	public void check(UnaryExpr e) {
		check(e.expr);
		Type type = e.expr.type;
		boolean matched = false;
		if (e.op.isSelfOp()) {
			//check Lvalue
			if (!e.expr.isLvalue)
				throw new Error("Invalid Lvalue");
			if (type instanceof Basic || type instanceof Pointer) {
				matched = true;
			}
			e.type = e.expr.type;
		} else if (e.op.optype == OpType.BIT_AND) {
			if (!e.expr.isLvalue)
				throw new Error("Invalid Lvalue");
			matched = true;
			e.type = new Pointer(type);
			e.isLvalue = false;
		} else if (e.op.optype == OpType.TIMES) {
			if (type instanceof Pointer) {
				matched = true;
				e.type = ((Pointer) type).elementType;
				e.isLvalue = true;
			} else if (type instanceof Array) {
				matched = true;
				e.type = ((Array) type).elementType;
				e.isLvalue = true;
			}
		} else if (e.op.optype == OpType.PAREN) {
			if (type instanceof Function) {
				if (((Function) type).argumentType.isEmpty()) {
					matched = true;
					e.type = ((Function) type).returnType;
					e.isLvalue =false;
				}
			}
		} else if (e.op.optype == OpType.PLUS || e.op.optype == OpType.MINUS || e.op.optype == OpType.NOT || e.op.optype == OpType.BIT_NOT) {
			if (type instanceof Basic) {
				matched = true;
				e.type = new Int();
				e.isLvalue = false;
				if (e.expr.isConstant()) {
					Integer val = null;
					if (e.expr.val instanceof Character)
						val = new Integer((int)((Character)e.expr.val).charValue());
					else if (e.expr.val instanceof Integer)
						val = new Integer((Integer)e.expr.val);
					if (val != null) {
						if (e.op.optype == OpType.PLUS)
							e.val = val;
						else if (e.op.optype == OpType.MINUS)
							e.val = -val;
						else if (e.op.optype == OpType.NOT)
							e.val = new Integer(val != 0 ? 0 : 1);
						else if (e.op.optype == OpType.BIT_NOT)
							e.val = new Integer(~val.intValue());
					}
				}
			}
		} else if (e.op.optype == OpType.SIZEOF) {
			matched = true;
			e.type = new Int();
			e.isLvalue = false;
			if (e.expr.isConstant()) {
				if (e.expr instanceof Chr)
					e.val = new Integer(1);
				else if (e.expr instanceof Num)
					e.val = new Integer(4);
				else if (e.expr instanceof Str)
					e.val = new Integer(((Array)e.type).capacity);
			}
		}
		if (!matched)
			throw new Error("Type Mismatched " + type + " " + e.op);
	}
	
	public void check(Constant e) {
		e.isLvalue = false;
//		e.type_specifier is pre-defined
	}
	
	public void check(Arg arg) {
		int size = 0;
		for (Expr e: arg.args) {
			check(e);
			size += e.type instanceof Array ? 4 : RecordField.align(e.type.size());
		}
		arg.size = size;
		arg.isLvalue = false;
//		arg.type undefined
	}
	
	public void check(Initializer it) {
		if (it instanceof ExprInitializer) {
			check(((ExprInitializer) it).expr);
			it.type = ((ExprInitializer) it).expr.type;
			it.isLvalue = it.isLvalue; // TODO check?
		} else if (it instanceof ArrayInitializer) {
			it.isLvalue = false;
			Type tmp = null;
			for (Initializer i : ((ArrayInitializer) it).list) {
				check(i);
				if (tmp == null) {
					tmp = i.type;
				} else if (!tmp.equals(i.type)) {
					throw new Error("Array Initilizer Type Mismatch " + tmp + " " + i.type);
				}
			}
			it.type = tmp;
		}
	}
	
	public void check(Var e) {
		// Undefined Variable
		if (vscope.get(e.sym) == null) {
			throw new Error("Undefined Variable " + e.sym);
		}
		e.type = ((VarInfo)vscope.get(e.sym)).type;
		e.isLvalue = !(e.type instanceof Array);
		//System.out.println(e.type.toString() + " " + e.sym);
	}
		
}
