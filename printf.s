
printf:
	sw $fp -4($sp) # frame base
	sw $ra -8($sp) # return addr
	sw $t0 -12($sp) # fmt_string_ptr
	sw $t1 -16($sp) # fmt_string_val
	sw $s0 -20($sp) # print_buffer
	sw $a1 -24($sp) # args
	move $fp $sp
	addiu $sp $sp -28
	move $s0 $sp
	sb $0 1($s0)
	lw $t0 0($fp)
	la $a1 0($fp)

.printf_loop:
	lb $t1 0($t0)
	addu $t0 $t0 1

	beq $t1 '%' .printf_fmt
	beq $t1 $0 _printf

.printf_putc:
	sb $t1 0($s0)
	move $a0 $s0
	li $v0 4	# print_str
	syscall
	b .printf_loop

.printf_fmt:
	# no %%, shift arg
	lb $t1 0($t0)
	addiu $t0 $t0 1
	addiu $a1 $a1 4
	beq $t1 'd' .printf_int
	beq $t1 'c' .printf_chr
	beq $t1 's' .printf_str

	# %04d
	# no other invalid input
	li $v0 1
	li $a0 0
	addiu $t0 $t0 3
	lw $t1 0($a1)
.oooo:
	bgt $t1 9 .ooo
	syscall
.ooo:
	bgt $t1 99 .oo
	syscall
.oo:
	bgt $t1 999 .o
	syscall
.o:
	move $a0 $t1
	syscall
	b .printf_loop

.printf_int:
	lw $a0 0($a1)
	li $v0 1 # print_int
	syscall
	b .printf_loop

.printf_chr:
	lb $t1 0($a1)
	sb $t1 0($s0)
	move $a0 $s0
	li $v0 4 # print_str
	syscall
	b .printf_loop

.printf_str:
	lw $a0 0($a1)
	li $v0 4 # print_str
	syscall
	b .printf_loop

_printf:
	addiu $sp $sp 28
	lw $fp -4($sp) # frame base
	lw $ra -8($sp) # return addr
	lw $t0 -12($sp) # fmt_string_ptr
	lw $t1 -16($sp) # fmt_string_val
	lw $s0 -20($sp) # print_buffer
	lw $a1 -24($sp) # args
	jr $ra
	nop
	nop
