#!/bin/bash
source finaltermvars.sh
cp data.c bin
cd bin
echo "COMPILING data.c"
$CCHK data.c 1>data.s
sleep 1
echo "RUNING data.s"
spim -stat -file data.s
echo "COMPILING data.c"
gcc data.c -m32 -o data
echo "RUNING data"
./data