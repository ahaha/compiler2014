all:
	mkdir -p bin
	cd src/tiger/syntactic && make
	javac src/tiger/*/*.java -classpath lib/java-cup-11a-runtime.jar:lib/gson-2.2.2.jar -d bin
	cp printf.s bin
	cp debug.s bin
	cp malloc.s bin
clean:
	cd src/tiger/syntactic && make clean
	rm -rf bin
