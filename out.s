.data
.newline: .asciiz "\n"
.S_1974097915: .asciiz "%d\n"
	
.text
.debug:
	move $v1 $v0
	move $t9 $a0
	li $v0 1
	syscall
	la $a0 .newline
	li $v0 4
	syscall
	move $a0 $t9
	move $v0 $v1
	jr $ra
	nop
	nop
	
	
printf:
	sw $fp -4($sp) # frame base
	sw $ra -8($sp) # return addr
	sw $t0 -12($sp) # fmt_string_ptr
	sw $t1 -16($sp) # fmt_string_val
	sw $s0 -20($sp) # print_buffer
	sw $a1 -24($sp) # args
	move $fp $sp
	addiu $sp $sp -28
	move $s0 $sp
	sb $0 1($s0)
	lw $t0 0($fp)
	la $a1 0($fp)
	
.printf_loop:
	lb $t1 0($t0)
	addu $t0 $t0 1
	
	beq $t1 '%' .printf_fmt
	beq $t1 $0 _printf
	
.printf_putc:
	sb $t1 0($s0)
	move $a0 $s0
	li $v0 4	# print_str
	syscall
	b .printf_loop
	
.printf_fmt:
	# no %%, shift arg
	lb $t1 0($t0)
	addiu $t0 $t0 1
	addiu $a1 $a1 4
	beq $t1 'd' .printf_int
	beq $t1 'c' .printf_chr
	beq $t1 's' .printf_str
	
	# %04d
	# no other invalid input
	li $v0 1
	li $a0 0
	addiu $t0 $t0 3
	lw $t1 0($a1)
.oooo:
	bgt $t1 9 .ooo
	syscall
.ooo:
	bgt $t1 99 .oo
	syscall
.oo:
	bgt $t1 999 .o
	syscall
.o:
	move $a0 $t1
	syscall
	b .printf_loop
	
.printf_int:
	lw $a0 0($a1)
	li $v0 1 # print_int
	syscall
	b .printf_loop
	
.printf_chr:
	lb $t1 0($a1)
	sb $t1 0($s0)
	move $a0 $s0
	li $v0 4 # print_str
	syscall
	b .printf_loop
	
.printf_str:
	lw $a0 0($a1)
	li $v0 4 # print_str
	syscall
	b .printf_loop
	
_printf:
	addiu $sp $sp 28
	lw $fp -4($sp) # frame base
	lw $ra -8($sp) # return addr
	lw $t0 -12($sp) # fmt_string_ptr
	lw $t1 -16($sp) # fmt_string_val
	lw $s0 -20($sp) # print_buffer
	lw $a1 -24($sp) # args
	jr $ra
	nop
	nop
	nop #main
main:
	sw $fp -4($sp)
	sw $ra -8($sp)
	move $fp $sp
	addiu $sp $sp -41352
	li $a0 0
	move $a0 $a0
	sw $a0 -40020($fp)
	jal .debug 			#	sum = 0
	li $a0 0
	move $a0 $a0
	sw $a0 -40016($fp)
	jal .debug 			#	i = 0
	b L_2
	jal .debug 			#	Jump L_2
	nop #L_1
L_1:
	li $a0 0
	move $a0 $a0
	sw $a0 -40012($fp)
	jal .debug 			#	j = 0
	b L_6
	jal .debug 			#	Jump L_6
	nop #L_5
L_5:
	lw $a0 -40016($fp)
	li $a1 400
	mul $a0 $a0 $a1
	sw $a0 -4($sp)
	jal .debug 			#	V_2 = i TIMES 400
	la $a0 -40008($fp)
	lw $a1 -4($sp)
	add $a0 $a0 $a1
	sw $a0 -8($sp)
	jal .debug 			#	V_1 = a PLUS V_2
	lw $a0 -40012($fp)
	li $a1 4
	mul $a0 $a0 $a1
	sw $a0 -12($sp)
	jal .debug 			#	V_5 = j TIMES 4
	lw $a0 -8($sp)
	lw $a1 -12($sp)
	add $a0 $a0 $a1
	sw $a0 -16($sp)
	jal .debug 			#	V_4 = V_3 PLUS V_5
	li $a0 0
	move $a0 $a0
	lw $a1 -16($sp)
	sw $a0 0($a1)
	jal .debug 			#	V_6 = 0
	nop #L_8
L_8:
	lw $a0 -40012($fp)
	li $a1 1
	add $a0 $a0 $a1
	sw $a0 -40012($fp)
	jal .debug 			#	V_7 = j PLUS 1
	nop #L_6
L_6:
	lw $a0 -40012($fp)
	li $a1 100
	blt $a0 $a1 L_5
	jal .debug 			#	Jump L_5 If j LT 100
	lw $a0 -40012($fp)
	li $a1 100
	bge $a0 $a1 L_7
	jal .debug 			#	Jump L_7 If j GE 100
	nop #L_7
L_7:
	nop #L_4
L_4:
	lw $a0 -40016($fp)
	li $a1 1
	add $a0 $a0 $a1
	sw $a0 -40016($fp)
	jal .debug 			#	V_8 = i PLUS 1
	nop #L_2
L_2:
	lw $a0 -40016($fp)
	li $a1 100
	blt $a0 $a1 L_1
	jal .debug 			#	Jump L_1 If i LT 100
	lw $a0 -40016($fp)
	li $a1 100
	bge $a0 $a1 L_3
	jal .debug 			#	Jump L_3 If i GE 100
	nop #L_3
L_3:
	li $a0 0
	move $a0 $a0
	sw $a0 -40016($fp)
	jal .debug 			#	i = 0
	b L_10
	jal .debug 			#	Jump L_10
	nop #L_9
L_9:
	lw $a0 -40016($fp)
	li $a1 20
	ble $a0 $a1 L_14
	jal .debug 			#	Jump L_14 If i LE 20
	lw $a0 -40016($fp)
	li $a1 80
	blt $a0 $a1 L_14
	jal .debug 			#	Jump L_14 If i LT 80
	lw $a0 -40016($fp)
	li $a1 80
	bge $a0 $a1 L_13
	jal .debug 			#	Jump L_13 If i GE 80
	nop #L_14
L_14:
	lw $a0 -40016($fp)
	li $a1 400
	mul $a0 $a0 $a1
	sw $a0 -20($sp)
	jal .debug 			#	V_10 = i TIMES 400
	la $a0 -40008($fp)
	lw $a1 -20($sp)
	add $a0 $a0 $a1
	sw $a0 -24($sp)
	jal .debug 			#	V_9 = a PLUS V_10
	lw $a0 -24($sp)
	lw $a0 0($a0)
	move $a0 $a0
	sw $a0 -40024($fp)
	jal .debug 			#	b = V_11
	li $a0 0
	move $a0 $a0
	sw $a0 -40012($fp)
	jal .debug 			#	j = 0
	b L_16
	jal .debug 			#	Jump L_16
	nop #L_15
L_15:
	lw $a0 -40012($fp)
	li $a1 5
	bgt $a0 $a1 L_20
	jal .debug 			#	Jump L_20 If j GT 5
	lw $a0 -40016($fp)
	li $a1 90
	blt $a0 $a1 L_20
	jal .debug 			#	Jump L_20 If i LT 90
	lw $a0 -40016($fp)
	li $a1 90
	bge $a0 $a1 L_19
	jal .debug 			#	Jump L_19 If i GE 90
	nop #L_20
L_20:
	lw $a0 -40012($fp)
	li $a1 4
	mul $a0 $a0 $a1
	sw $a0 -28($sp)
	jal .debug 			#	V_13 = j TIMES 4
	lw $a0 -40024($fp)
	lw $a1 -28($sp)
	add $a0 $a0 $a1
	sw $a0 -32($sp)
	jal .debug 			#	V_12 = b PLUS V_13
	la $a0 -32($sp)
	lw $a0 0($a0)
	sw $a0 -36($sp)
	jal .debug 			#	V_14 = MEMOF V_12
	li $a0 100
	li $a1 1
	sub $a0 $a0 $a1
	sw $a0 -40($sp)
	jal .debug 			#	V_16 = 100 MINUS 1
	lw $a0 -40($sp)
	li $a1 1
	add $a0 $a0 $a1
	sw $a0 -44($sp)
	jal .debug 			#	V_17 = V_16 PLUS 1
	lw $a0 -44($sp)
	li $a1 1
	sub $a0 $a0 $a1
	sw $a0 -48($sp)
	jal .debug 			#	V_18 = V_17 MINUS 1
	lw $a0 -48($sp)
	li $a1 1
	add $a0 $a0 $a1
	sw $a0 -52($sp)
	jal .debug 			#	V_19 = V_18 PLUS 1
	lw $a0 -52($sp)
	li $a1 2
	div $a0 $a1
	mflo $a0
	sw $a0 -56($sp)
	jal .debug 			#	V_20 = V_19 DIVIDE 2
	lw $a0 -40012($fp)
	lw $a1 -56($sp)
	add $a0 $a0 $a1
	sw $a0 -60($sp)
	jal .debug 			#	V_15 = j PLUS V_20
	lw $a0 -60($sp)
	move $a0 $a0
	sw $a0 -64($sp)
	jal .debug 			#	V_14 = V_15
	nop #L_19
L_19:
	nop #L_18
L_18:
	lw $a0 -40012($fp)
	li $a1 1
	add $a0 $a0 $a1
	sw $a0 -40012($fp)
	jal .debug 			#	V_21 = j PLUS 1
	nop #L_16
L_16:
	lw $a0 -40012($fp)
	li $a1 100
	blt $a0 $a1 L_15
	jal .debug 			#	Jump L_15 If j LT 100
	lw $a0 -40012($fp)
	li $a1 100
	bge $a0 $a1 L_17
	jal .debug 			#	Jump L_17 If j GE 100
	nop #L_17
L_17:
	nop #L_13
L_13:
	nop #L_12
L_12:
	lw $a0 -40016($fp)
	li $a1 1
	add $a0 $a0 $a1
	sw $a0 -40016($fp)
	jal .debug 			#	V_22 = i PLUS 1
	nop #L_10
L_10:
	lw $a0 -40016($fp)
	li $a1 100
	blt $a0 $a1 L_9
	jal .debug 			#	Jump L_9 If i LT 100
	lw $a0 -40016($fp)
	li $a1 100
	bge $a0 $a1 L_11
	jal .debug 			#	Jump L_11 If i GE 100
	nop #L_11
L_11:
	li $a0 0
	move $a0 $a0
	sw $a0 -40016($fp)
	jal .debug 			#	i = 0
	b L_22
	jal .debug 			#	Jump L_22
	nop #L_21
L_21:
	li $a0 0
	move $a0 $a0
	sw $a0 -40012($fp)
	jal .debug 			#	j = 0
	b L_26
	jal .debug 			#	Jump L_26
	nop #L_25
L_25:
	lw $a0 -40016($fp)
	li $a1 400
	mul $a0 $a0 $a1
	sw $a0 -68($sp)
	jal .debug 			#	V_24 = i TIMES 400
	la $a0 -40008($fp)
	lw $a1 -68($sp)
	add $a0 $a0 $a1
	sw $a0 -72($sp)
	jal .debug 			#	V_23 = a PLUS V_24
	lw $a0 -40012($fp)
	li $a1 4
	mul $a0 $a0 $a1
	sw $a0 -76($sp)
	jal .debug 			#	V_27 = j TIMES 4
	lw $a0 -72($sp)
	lw $a1 -76($sp)
	add $a0 $a0 $a1
	sw $a0 -80($sp)
	jal .debug 			#	V_26 = V_25 PLUS V_27
	lw $a0 -40020($fp)
	lw $a1 -80($sp)
	lw $a1 0($a1)
	add $a0 $a0 $a1
	sw $a0 -40020($fp)
	jal .debug 			#	sum = sum PLUS V_28
	nop #L_28
L_28:
	lw $a0 -40012($fp)
	li $a1 1
	add $a0 $a0 $a1
	sw $a0 -40012($fp)
	jal .debug 			#	V_29 = j PLUS 1
	nop #L_26
L_26:
	lw $a0 -40012($fp)
	li $a1 100
	blt $a0 $a1 L_25
	jal .debug 			#	Jump L_25 If j LT 100
	lw $a0 -40012($fp)
	li $a1 100
	bge $a0 $a1 L_27
	jal .debug 			#	Jump L_27 If j GE 100
	nop #L_27
L_27:
	nop #L_24
L_24:
	lw $a0 -40016($fp)
	li $a1 1
	add $a0 $a0 $a1
	sw $a0 -40016($fp)
	jal .debug 			#	V_30 = i PLUS 1
	nop #L_22
L_22:
	lw $a0 -40016($fp)
	li $a1 100
	blt $a0 $a1 L_21
	jal .debug 			#	Jump L_21 If i LT 100
	lw $a0 -40016($fp)
	li $a1 100
	bge $a0 $a1 L_23
	jal .debug 			#	Jump L_23 If i GE 100
	nop #L_23
L_23:
	la $a2 .S_1974097915
	sw $a2 0($sp)
	lw $a3 -40020($fp)
	sw $a3 4($sp)
	jal printf
	move $a0 $v0
	move $a0 $a0
	sw $a0 -84($sp)
	jal .debug 			#	V_31 = RetVal
	nop #_main
_main:
	addiu $sp $sp 41352
	lw $fp -4($sp)
	lw $ra -8($sp)
	jr $ra
	nop
	nop
	
	
	

